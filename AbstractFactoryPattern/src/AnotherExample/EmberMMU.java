package AnotherExample;

/*
 * A concrete MMU product belonging to the "Ember" category.
 * 
 * (All objects belonging to the Ember category across ALL products is defined as a FAMILY)
 */

public class EmberMMU extends MMU {

	@Override
	public String toString() {
		return "EmberMMU object";
	}
	
}
