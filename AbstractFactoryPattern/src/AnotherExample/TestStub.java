package AnotherExample;

public class TestStub {

	public static void main(String[] args) {
		
		/*
		 * Create our factory (this would be passed into client's constructor for aggregation, or created for composition)
		 * NOTE: Concrete factories are only instantiated in one place, therefore to switch between product families, 
		 * 			simply replace the architecture specification as needed to get the correct concreteFactory.
		 */
		
//		ArchitectureToolKit toolKit = EmberToolKit.getEmberToolKit();
		ArchitectureToolKit toolKit = EnigmaToolKit.getEnigmaToolKit();
		
		//-----------------------------------------------------------------------------------------------------------------------------
		
		/*
		 * these can be an Enigma or Ember Architecture, simply based on what concrete factory (toolKit) is used.
		 */
		CPU someArchCPU = toolKit.createCPU(); 
		MMU someArchMMU = toolKit.createMMU();
		
		System.out.println(someArchCPU.toString());
		System.out.println(someArchMMU.toString());
	}

}
