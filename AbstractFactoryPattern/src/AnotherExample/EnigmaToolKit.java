package AnotherExample;

/*
 * A concrete factory, encapsulates all knowledge about and is responsible for creating one FAMILY of products.
 * It is usually a SINGLETON, created by eager instantiation in the Abstract Factory and used as needed.
 * (meaning it's created when the class is loaded, or in a static initialization block; before any thread can access it)
 * 
 * Recall: A family of products can be thought of as "all objects of one category across all products".
 */

public class EnigmaToolKit implements ArchitectureToolKit {

	//eager-instantiation of singletons
	private static final EnigmaToolKit enigmaToolKit = new EnigmaToolKit();
	
	public static EnigmaToolKit getEnigmaToolKit(){
		return enigmaToolKit;
	}
	
	private EnigmaToolKit() {}
	
	@Override
	public CPU createCPU() {
		return new EnigmaCPU();
	}

	@Override
	public MMU createMMU() {
		return new EnigmaMMU();
	}

}
