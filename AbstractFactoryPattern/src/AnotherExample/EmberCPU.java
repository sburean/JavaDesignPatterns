package AnotherExample;

/*
 * A concrete CPU product belonging to the "Ember" category.
 * 
 * (All objects belonging to the Ember category across ALL products is defined as a FAMILY)
 */

public class EmberCPU extends CPU {

	@Override
	public String toString() {
		return "EmberCPU object";
	}
	
}
