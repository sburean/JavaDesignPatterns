package AnotherExample;

/*
 * A concrete MMU product belonging to the "Enigma" category.
 * 
 * (All objects belonging to the Enigma category across ALL products is defined as a FAMILY)
 */

public class EnigmaMMU extends MMU {

	@Override
	public String toString() {
		return "EnigmaMMU object";
	}
	
}
