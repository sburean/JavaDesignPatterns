package AnotherExample;

/*
 * The abstract factory; it contains one abstract factory method PER product. (so 2 in this example)
 */

public interface ArchitectureToolKit {

	// factory methods for object instantiation - one PER PRODUCT!
	CPU createCPU();
	MMU createMMU();

}
