package AnotherExample;

/*
 * A concrete CPU product belonging to the "Enigma" category.
 * 
 * (All objects belonging to the Enigma category across ALL products is defined as a FAMILY)
 */

public class EnigmaCPU extends CPU {

	@Override
	public String toString() {
		return "EnigmaCPU object";
	}
	
}
