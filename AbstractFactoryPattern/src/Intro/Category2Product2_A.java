package Intro;
/*
 * A concrete product belonging to category1 of Product2SuperType.
 * It has multiple variations as defined by the Enum: "Variation" 
 */

public class Category2Product2_A implements Product2SuperType {

	Variation categoryVariation;
	
	public Category2Product2_A(Variation variation) {
		this.categoryVariation = variation;
	}

	@Override
	public void method2() {
		System.out.println("Category2Product2_A");
	}


}
