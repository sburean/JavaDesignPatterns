package Intro;
/*
 * One product super-type.  All products that are to be created by the factory pattern must have a super-type. 
 * NOTE: Categories between product super-types must be consistent!
 */

public interface Product1SuperType {

	abstract public void method1();
	
}
