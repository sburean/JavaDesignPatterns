package Intro;
/*
 * The "client". It is decoupled from the concrete products; only refers to factories and products by their super-type.
 * The Abstract Factory "Decouples concrete products from objects that use them". Implementation details of concrete products is not exposed to the client. 
 * NOTE: See that the client only relies about super-types. (decoupled from concrete implementation of products)
 * 
 * The client is written against an abstract factory and composed with an actual concrete factory at runtime. (Through the constructor)
 * It creates objects that are a part of a theme (based on the concrete factory it's composed with)
 * "Delegates object creation to the composed factory"
 * 
 * Abstract Factory creates objects through composition; object creation is implemented in methods exposed by factory interface. 
 * Object composition is a strong HAS-A association relationship. It is OWNERSHIP. 
 * Therefore, the concreteFactories are owned by the client; they get created in the client's constructor and share it's lifecycle. 
 * 
 */

public class Client {

	FactorySupertype factory; //The factory that the client will delegate object instantiation to. 
	
	public Client(){
		//Client is COMPOSED with a factory at runtime. Strong HAS-A relationship (ownership).
		
		/* Use or change factories as appropriate */
		this.factory = new ConcreteFactory1(); 
//		this.factory = new ConcreteFactory2(); 
	}
	
	public void generateProducts(){
		
		//create each product from a factory, and call their method (which prints their description)
		
		Product1SuperType a = factory.createProduct1(Variation.A);
		a.method1();
		
		//etc.. (the rest don't declare a product super type)
		factory.createProduct1(Variation.B).method1();
		factory.createProduct2(Variation.A).method2();
		factory.createProduct2(Variation.B).method2();
	}
	

}
