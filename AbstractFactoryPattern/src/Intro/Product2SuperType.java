package Intro;
/*
 * Another product super-type.  All products that are to be created by the factory pattern must have a super-type. 
 * NOTE: Categories between product super-types must be consistent!
 */

public interface Product2SuperType {

	abstract public void method2();
	
}
