package Intro;
/*
 * The test-stub creates a client, and calls on the client to perform some action. 
 * (In this case, the client delegates object creation to the factory it's composed with, and calls a product method)
 */

public class TestStub {

	public static void main(String[] args) {
		
		Client client = new Client(); //create a client; it's composed with a factory in it's constructor. 
		
		client.generateProducts();
		
	}

}
