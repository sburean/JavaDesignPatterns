package Intro;
/*
 * To differentiate between variations within categories of "Product1SuperType" & "Product2SuperType"
 */

public enum Variation {
	A, B
}
