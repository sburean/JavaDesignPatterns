package Intro;

/*
 * Abstract Factory provides a way to encapsulate a GROUP of individual factories that have a common theme. (ie: products have similar categories) 
 * -> It is one level of abstraction higher than the Factory Method. 
 * 
 * Abstract Factory can create products of multiple SuperTypes, each sharing similar categories. (with many variations within each category) 
 * 
 * IMPORTANT NOTE: There is a ratio of 1:1 between Product SuperTypes and Factory Methods defined in the FactorySupertype 
 * 					AND 1:1 between ConcreteFactories and product Categories (like the FactoryMethod pattern) 
 * 		 
 * -> Abstract Factory groups together a set of related products.
 * 		This interface will change if new product types are introduced !
 * 
 */

public interface FactorySupertype {
	
	//Note the 1:1 ratio between the number of FactorySupertype Methods and Product Super-Types.
	public Product1SuperType createProduct1(Variation variation);
	public Product2SuperType createProduct2(Variation variation);
	
	/*
	 * Instead of having the variation Enum, we can simply have multiple methods returning different (variant) concrete products of the same super-type (any possibly different categories):
	 * (This separates the factoryMethods, and prevents the need to use switch or if-else conditionals to differentiate between product variations)
	 * 
	 * SEE COMPOUND PATTERN PROJECT - 'AbstractDuckFactory'!
	 */
	
//	public Product1SuperType createCategory1Product1A();
//	public Product1SuperType createCategory1Product1B();
//	public Product1SuperType createCategory2Product1A();
//	public Product1SuperType createCategory2Product1B();
	
	
	/* 
	 	In addition to the factory method...
	 		We could also have the FactorySuperType an abstract class containing a switch statement 
	    	that uses returns the appropriate concreteFactory based on a type. (Passed into the superType constructor)
	    	Would have public static final  method that return a new concreteFactory instance.
	    	
	    	This way, the client would only have to refer to the FactorySuperType's -
	    	- static methods with appropriate parameters to get our appropriate factories. 
	      
	    --> See example here: https://sourcemaking.com/design_patterns/abstract_factory/java/1
	    
	    NOTE: See how it still contains concreteFactories (EmberToolkit) that creates ONE category (Ember/Engino architecture)
	    		of a product family (CPU/MMU/etc...)
	    
	 */
	
	

}