package Intro;
/*
 * A concrete factory. It encapsulates all knowledge about, and is responsible for creating -
 * - all products belonging to category 1 (and all variations within) OF ALL SUPERTYPES!
 * Recall the 1:1 ratio between concrete factory methods to product super-types. 
 * 
 * Clients are COMPOSED with these concrete factories and delegate product creation to the concrete factory.
 * 
 */
public class ConcreteFactory1 implements FactorySupertype {

	@Override
	public Product1SuperType createProduct1(Variation variation) {
		//Factory METHOD for product 1
		switch(variation){
			case A:
				return new Category1Product1_A(Variation.A);
			case B:
				return new Category1Product1_B(Variation.B);
			default:
				throw new IllegalArgumentException("Error: No product available in category 1 - product 1 for specified variation!");
		}
	}

	@Override
	public Product2SuperType createProduct2(Variation variation) {
		//Factory METHOD for product 2
		switch(variation){
			case A:
				return new Category1Product2_A(Variation.A);
			case B:
				return new Category1Product2_B(Variation.B);
			default:
				throw new IllegalArgumentException("Error: No product available in category 1 - product 2 for specified variation!");
		}
	}

}
