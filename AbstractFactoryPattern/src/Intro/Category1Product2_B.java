package Intro;
/*
 * A concrete product belonging to category1 of Product2SuperType.
 * It has multiple variations as defined by the Enum: "Variation" 
 */

public class Category1Product2_B implements Product2SuperType {

	Variation categoryVariation;
	
	public Category1Product2_B(Variation variation) {
		this.categoryVariation = variation;
	}

	@Override
	public void method2() {
		System.out.println("Category1Product2_B");
	}


}
