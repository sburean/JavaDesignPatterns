package Example;

public class AnimalMaker {

	/**
	 * Our client: 
	 * Make animals of a certain family; use factory functionality (through composition)
	 * 
	 * It uses a factory super-type to delegate object instantiation.  
	 * That's how we use composition to create objects. 
	 * 
	 * Then it uses those objects to do something with them. 
	 *  
	 *  
	 *  (Equivalent to "pizza store" in the HeadFirst book)
	 */
	
	AnimalFactory familyFactory; 
	
	public AnimalMaker(AnimalFactory familyFactory){
		this.familyFactory = familyFactory; //this defines the product family that this client will deal with. 
	}
	
	public Bird makeBird() {
		return familyFactory.createBird(); //composition to instantiate object of the specified family.
	}
	
	public Feline makeFeline(){
		return familyFactory.createFeline(); //composition to instantiate object of the specified family.
	}
	
	//Other methods..
	
}
