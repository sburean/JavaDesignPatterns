package Example;

public class HouseFriendlyAnimalFactory implements AnimalFactory {

	//Family: house friendly products
	
	/** 
	 *  This is a concrete factory implementation that creates a FAMILY of products -
	 *  - the family of house-friendly animals.
	 *  (ie: ALL products that contain family members of the family "house-friendly")
	 * 
	 *  Again, this is the ONLY class that deals with creating families of products of this KIND.
 	 *  REMEMBER: Any and All variations of these product KINDs!
	 */
	
	//Create a family of products:
	
	@Override
	public Feline createFeline() {
		return new Cat(); //one house friendly product
		//could have a "FurryCat" as well ( can have whole family of house-friendly felines )
	}

	@Override
	public Bird createBird() {
		return new Parrot();
		//could have a "canary"; a member of the family of house-friendly birds) 
	}

}
