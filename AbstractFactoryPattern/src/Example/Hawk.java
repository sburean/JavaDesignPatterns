package Example;

public class Hawk implements Bird {

	/**
	 * Concrete product class of super-type "bird"
	 * 
	 * This kind of bird is NOT house-friendly. (member of the NOT house-friendly family of birds)
	 */
	
	@Override
	public void fly() {
		System.out.println("Hawks fly quickly!");
	}

}
