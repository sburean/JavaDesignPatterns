package Example;

public class Cat implements Feline {

	/**
	 * Concrete product class of super-type "feline".
	 * 
	 * This kind of feline is house-friendly.  (member of the house-friendly family of felines)
	 */
	
	@Override
	public void meow() {
		System.out.println("The cat says meow.");
	}

}
