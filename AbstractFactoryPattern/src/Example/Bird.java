package Example;

public interface Bird {

	/**
	 * - Abstract Product Interface
	 * Our super-type of 'Bird'. This is one product, it can contain different kinds of birds. (ie: multiple families)
	 * 
	 * Terminology: Family = Kinds
	 * 
	 * IMPORTANT: The concrete factories instantiate concrete objects of a certain KINDS!
	 * --> see: http://stackoverflow.com/a/20648551 "ConcreteFactory1 & ConcreteFactory2" for 2 kinds/families of products.
	 * (and then within those concrete factories, can create all variations of 
	 * those product kinds; like the factory method)
	 */
	
	public void fly();
	
}
