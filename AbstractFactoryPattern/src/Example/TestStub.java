package Example;

public class TestStub {

	public static void main(String[] args) {
		
		//Create factories:
		AnimalFactory petFactory = new HouseFriendlyAnimalFactory();
		AnimalFactory meanFactory = new NonHouseFriendlyAnimalFactory();
		
		//Make a client
		AnimalMaker petClients = new AnimalMaker(petFactory); //this client only makes friendly pets because we're passing in a petFactory!
		AnimalMaker badClients = new AnimalMaker(meanFactory);
		
		//Now make our objects
		Bird bird1 = petClients.makeBird();
		Bird bird2 = badClients.makeBird();
		
		Feline meower1 = petClients.makeFeline();
		Feline meower2 = badClients.makeFeline();
		
		//Do stuff..
		
		bird1.fly();
		bird2.fly();
		
		System.out.println("");
		
		meower1.meow();
		meower2.meow();

	}

}
