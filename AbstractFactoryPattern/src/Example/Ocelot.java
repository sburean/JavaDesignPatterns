package Example;

public class Ocelot implements Feline {

	/**
	 * Concrete product class of super-type "feline"
	 * 
	 * This kind of feline is NOT house-friendly. (member of the NOT house-friendly family of felines)
	 */
	
	@Override
	public void meow() {
		System.out.println("The Ocelot says roar!");
	}

}
