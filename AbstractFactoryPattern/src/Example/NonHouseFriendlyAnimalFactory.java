package Example;

public class NonHouseFriendlyAnimalFactory implements AnimalFactory {

	//Family: non-house friendly products
	
	/** 
	 *  This is a concrete factory implementation that creates MULTIPLE products, -
	 *  - that are members of the NON-house friendly FAMILY!
	 * 
	 *  Again, this is the ONLY class that deals with creating products of this family. 
 	 *  REMEMBER: Any and All variations of these product KINDs!
	 */
	
	//Create family of products:
	
	@Override
	public Feline createFeline() {
		return new Ocelot();
		//Could have : return new Tiger(), etc... ( or any member of the non-house friendly felines ) 
	}

	@Override
	public Bird createBird() {
		return new Hawk();
		//Could have : return new Eagle(), etc.. (again, can return any member of the non-house friendly birds)
	}

}
