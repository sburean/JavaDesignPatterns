package Example;

public class Parrot implements Bird {

	/**
	 * Concrete product class of super-type "bird"
	 * 
	 * This kind of bird is house-friendly. (member of the house-friendly family of birds)
	 */
	
	@Override
	public void fly() {
		System.out.println("Parrots fly, but they can also speak!");
	}

}
