package Example;

public interface AnimalFactory {

	/*
	 * This is our abstract factory interface. The abstract factory pattern crates families/kinds of multiple products. 
	 * The pattern also decouples the client from concrete products by using products (objects) -
	 * - of super-type in the concrete factories. 
	 * 
	 * 
	 * NOTE: The abstract factory method creates objects through COMPOSITION, and consists of:
	 * ---------------------------------
	 * 	1: abstract factory interface
	 * 	2: concrete factory classes
	 * 	3: client 
	 * (This is what the abstract factory pattern deals with)
	 * ---------------------------------
	 * 	4: abstract product interface
	 * 	5: concrete product classes
	 * (Products are from our program, abstract factory doesn't deal with their design)
	 * ---------------------------------
	 */
	
	/*
	 * IMPORTANT: The abstract factory interface is a container for several factory methods that create MULTIPLE products. 
	 */
	
	
	//IMPORTANT - NOTE #1 : the factory methods correspond to the product interfaces !
	
	public Feline createFeline(); //Create one product (may contain different product families/kinds)
	public Bird createBird(); //Create another product (may contain different product families/kinds)
	
	/*
	 * IMPORTANT - NOTE: 
	 * As per the Factory Method Pattern, there is one concrete factory PER PRODUCT FAMILY! 
	 * In this case, one factory for a family of House-Friendly animals, & another factory for a 
	 * family of NotHouse-Friendly animals. 
	 * 
	 * These creators create the WHOLE FAMILY of those objects (see classes mentioned above)
	 */
	
	
}
