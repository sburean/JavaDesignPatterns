import java.util.Random;

/*
 * A state class (concrete implementation of the State superType). It encapsulates all information about a state. It implements behaviours that are appropriate for this state!
 * This state class will be responsible for the client's (The StateMachine) behaviour while it is in this state through delegation. (composition)
 * By behaviour, I mean the actions that it performs.
 */

public class HasQuarterState implements State {

	GumballMachine gumballMachine;
	
	public HasQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}
	
	@Override
	public void insertQuarter() {
		// Can't insert a quarter when there's already one in there.
		System.out.println("You can't insert another quarter.");
	}

	@Override
	public void ejectQuarter() {
		// gives customer back quarter and moves state-machine to NoQuarterState.
		System.out.println("Quarter returned");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}

	@Override
	public void turnCrank() {
		
		//NOTE: Since we're initializing the gumBallMachine in SoldOutState unless we set a count > 0, this will always have a count > 0

		// Takes quarter, and moves to either Winner(10% of the time) or Sold(90% of the time) states.
		System.out.println("You turned the crank...");
		Random rng = new Random();
		if( rng.nextInt(10) == 1){ //random number bound between: [0,10)
			/* 10% of the time; customer gets two gum-balls for their quarter */
			gumballMachine.setState(gumballMachine.getWinnerState());
		} else {
			/* 90% of the time; customer gets one gum-ball for their quarter */
			gumballMachine.setState(gumballMachine.getSoldState());
		}

	}

	@Override
	public void dispense() {
		// Can't dispense without turning crank
		System.out.println("You need to turn the crank first.");
	}

	@Override
	public void refill(int count) {
		// Machine not empty; can't refill.
		System.out.println("Machine is not empty! Can't refill");
		
	}

}
