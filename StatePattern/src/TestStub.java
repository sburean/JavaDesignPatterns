

/*
 * The testStub for the StatePattern simply instantiates a context object and calls it's action methods to perform state transitions.
 * The context object delegates these action requests to its currently referenced state object (that represents it's internal state) 
 */

public class TestStub {

	public static void main(String[] args) {

		//Create context
		GumballMachine gumballMachine = new GumballMachine(5);
		
		System.out.println(gumballMachine);
		
		//Call action methods on the context (perform state transitions)
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		
		System.out.println(gumballMachine);
		
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		
		System.out.println(gumballMachine);
		
	}

}
