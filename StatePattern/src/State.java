/*
 * An interface for the states. It will be implemented by all classes representing states.
 * It contains methods for all possible actions within the state-machine.
 * 
 * Concrete State objects may be shared across multiple instances of context. Just assign each state to a static instance variable in the context class.
 * Can then share them across multiple contexts, HOWEVER, concrete states MAY NOT keep internal state otherwise it won't work properly.
 * 
 *  NOTE: Have a lot of duplicate code across multiple concrete states using this interface; would be better to use an abstract class and define default behaviour.
 */

public interface State {

	public void insertQuarter();
	
	public void ejectQuarter();
	
	public void turnCrank();
	
	public void dispense(); // NOTE: This is not an actual action performed externally, it is an internal action performed directly by the state-machine.
	
	public void refill(int count);
	
}