package RemoteProxy;
/*
 * A state class (concrete implementation of the State superType). It encapsulates all information about a state. It implements behaviours that are appropriate for this state!
 * This state class will be responsible for the client's (The StateMachine) behaviour while it is in this state through delegation. (composition)
 * By behaviour, I mean the actions that it performs.
 */

public class SoldOutState implements State {

	//Mark as transient as we don't want to serialize state of these objects (set via constructor)
	transient GumballMachine gumballMachine;
	
	public SoldOutState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}
	
	@Override
	public void insertQuarter() {
		// Machine is sold out of gum-balls, won't accept more quarters.
		System.out.println("You can't insert a new quarter, the machine is sold out.");
	}

	@Override
	public void ejectQuarter() {
		// Machine is sold out of gum-balls, doesn't accept quarters, therefore nothing to eject.
		System.out.println("You can't eject, you haven't inserted a quarter yet.");
	}

	@Override
	public void turnCrank() {
		// Machine is sold out of gum-balls, turning crank won't do anything.
		System.out.println("You turned, but there's no quarter.");
	}

	@Override
	public void dispense() {
		// Machine is sold out of gum-balls, can't dispense anything.
		System.out.println("You need to pay first.");
	}

	@Override
	public void refill(int count) {
		// Add more gum-balls and move to the "NoQuarterState"
		/*
		 *  NOTE: Can simply have a refill(int x) method in the StateMachine, and can call it from a method here. 
		 *	->Prevents adding a new method to each concrete state; but just did this to show that it's a state transition.
		 */
		gumballMachine.setCount(count);
		System.out.println("Machine refilled with " + count + " gumballs!");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}
	
	@Override
	public String toString(){
		return "machine is sold out !";
	}
	

}
