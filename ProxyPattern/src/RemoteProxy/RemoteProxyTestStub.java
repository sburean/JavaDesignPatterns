package RemoteProxy;

/*
 * A testStub for the RemoteProxy:
 * - Gets a reference to the RemoteProxy (RMI-STUB) via the java.rmi.Naming.lookup(..) method.
 * - Instantiates a client, passing the RMI-STUB into its constructor
 * - Calls methods in client that internally call remote methods via the stub  
 */

public class RemoteProxyTestStub {

	public static void main(String[] args) {
		
		try {
			
			//First, retrieve the RemoteProxy (RMI-STUB object client will call methods on)
			RemoteInterface remote = (RemoteInterface) java.rmi.Naming.lookup("remoteMachineToronto"); 
			//NOTE: argument must be the same as 1st parameter used in Naming.rebind(..) to get appropriate RMI-STUB!
					
			//Now create the monitor, passing in the RMI-STUB of the remote service whose methods we will be calling ( & just got via lookup() )
			GumballMonitor monitor = new GumballMonitor(remote);
			
			//And call some client method that internally calls remote methods:
			monitor.report();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
