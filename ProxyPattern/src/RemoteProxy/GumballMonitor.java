package RemoteProxy;

import java.rmi.RemoteException;

/*
 * The client class that prints a gumballMachine's information. (GumballMachines will be remote services)
 * This is the client for the RemoteProxy (RMI-STUB); it calls methods on a remote service interface through a RMI-STUB.
 * 
 *  IMPORTANT:
 *  - We need to have the RMI-STUB .class file (generated on the remote HEAP) before we do the lookup.
 *  		(lookup returns a serialized RMI-STUB object and automatically de-serializes on the client HEAP; 
 *  			this will will fail without corresponding .class file) 
 *  - For small projects, simply copying the .class file over is fine, but for larger systems: DyanmicClassDownloading.
 *  
 *  NOTE:
 *  - See that the client (this monitor class) doesn't care (or know) that the GumBall Machine is a remote object.
 *   -> It only has to worry about handing the exceptions that some methods may throw.
 */

public class GumballMonitor {

	//IMPORTANT: Client uses the REMOTE INTERFACE to refer to the remote service: (so it doesn't care about concrete remote classes)
	RemoteInterface remoteGumballMachine;
	
	public GumballMonitor(RemoteInterface remote) {
		//Constructor takes an RMI-STUB object (from java.rmi.Naming.lookup) to call methods on; it implements the remoteInterface
		this.remoteGumballMachine = remote;
	}
	
	public void report(){
		try {
			//See that the client has to acknowledge the risks (exceptions) with NW I/O and handle them: (via try/catch)
			System.out.println("Gumball Machine: " + remoteGumballMachine.getLocation()); 
			System.out.println("Current inventory: " + remoteGumballMachine.getCount() + " gumballs"); 
			System.out.println("Current state: " + remoteGumballMachine.getState());	
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}

