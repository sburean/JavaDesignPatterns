package RemoteProxy;

import java.io.Serializable;
/*
 * An interface for the states. It will be implemented by all classes representing states.
 * It contains methods for all possible actions within the state-machine.
 * 
 * Concrete State objects may be shared across multiple instances of context. Just assign each state to a static instance variable in the context class.
 * Can then share them across multiple contexts, HOWEVER, concrete states MAY NOT keep internal state otherwise it won't work properly.
 * 
 *  NOTE: Have a lot of duplicate code across multiple concrete states using this interface; would be better to use an abstract class and define default behaviour.
 *  
 *  IMPORTANT:
 *  - This superType is a return type of a remote method. It needs to be Serializable!
 *  - Need to extend java.io.Serializable interface; has no methods and allows objects that implement
 *  	 this interface to be serializable. (and transfered over the network)
 *  - Also have to mark state (data fields) of objects implementing this interface as "Transient" if we don't
 *  	 want some of them to be serialized (persisted).
 */

public interface State extends Serializable {

	public void insertQuarter();
	
	public void ejectQuarter();
	
	public void turnCrank();
	
	public void dispense(); // NOTE: This is not an actual action performed externally, it is an internal action performed directly by the state-machine.
	
	public void refill(int count);
	
}