package RemoteProxy;
/*
 * The remote interface for our ProxyPattern. It will be exposed by the RemoteService.
 * Implemented by the remoteObject and used by the client.
 * Contains methods a client may call remotely. 
 * -> Each method MUST throw a RemoteException. Client will have to acknowledge these since NW I/O is risky.
 * -> Each method parameter or return type must be SERIALIZABLE / primitives. (Especially important with custom objects) 
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteInterface extends Remote {
	
	/*
	 * NOTE: Better to have these three methods instead of one "report()" method because would have code -
	 * 	- duplication across each gumballMachine.
	 */
	
	public int getCount() throws RemoteException;
	public String getLocation() throws RemoteException;
	public State getState() throws RemoteException; //NOTE: A custom object, need to make serializable!

}
