package RemoteProxy;
/*
 * The launcher-class for the remote service: (GumballMachine)
 * 	- Instantiates remote service object.
 * 	- Registers it with the RMI registry.
 * 	- Starts the remote service.  
 * 
 * NOTE: Must run rmic to create stub/skeleton classes, AND THEN rmiregistry in the terminal first to ensure rebinding/lookup works!!! 
 * 
 * RUN-EXTERNAL-TOOLS: (on windows)
 * rmic: working directory = project/bin, AND args = package.serviceName (without the .class)
 * rmiregistry: working directory = project/bin, AND args = port #
 */

public class RemoteServiceLauncher {

	public static void main(String[] args) {
		
		try { //Need to surround instantiation of remoteService with try/catch because constructor can throw exception
			
			GumballMachine gumballMachine = new GumballMachine("Toronto", 100); //instantiate service object
			
			//Line below makes things easier on Mac if already have .class files for stub.
			java.rmi.registry.LocateRegistry.createRegistry(1099); //starts a rmi registry on port 2001, OR use the "run external tools" option
			
			/*
			 * Registers the object specified as the second parameter in the rebind method into the RMI registry, 
			 * 	but SWAPS the object with it's corresponding RMI-STUB.
			 * So an RMI-SUB object is actually stored in the RMI Registry. (Serialized) 
			 */
			java.rmi.Naming.rebind("remoteMachineToronto", gumballMachine);
			
			System.out.println("service running..");
			
//			gumballMachine.insertQuarter();
//			gumballMachine.turnCrank();
//			gumballMachine.insertQuarter();
//			gumballMachine.turnCrank();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
