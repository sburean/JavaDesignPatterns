package RemoteProxy;
/*
 * A state class (concrete implementation of the State superType). It encapsulates all information about a state. It implements behaviours that are appropriate for this state!
 * This state class will be responsible for the client's (The StateMachine) behaviour while it is in this state through delegation. (composition)
 * By behaviour, I mean the actions that it performs.
 * 
 * NOTE: In this example, the SoldState represents the customer getting ONE gum-ball for their quarter. (We arrive here 90% of the time; as chance to win a free one is 10%)
 * 
 * IMPORTANT: Could have put logic for dispensing two gum-balls here (if customer is winner) but that violates then "One class, One responsibility" principle. 
 * 				Also opens this state class up for modification (if promotion ends, etc..)
 */

public class SoldState implements State {

	//Mark as transient as we don't want to serialize state of these objects (set via constructor)
	transient GumballMachine gumballMachine;
	
	public SoldState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}
	
	@Override
	public void insertQuarter() {
		// Currently dispensing gum-ball, wait before inserting quarter again.
		System.out.println("Please wait, we're already giving you a gum-ball");
	}

	@Override
	public void ejectQuarter() {
		// No quarter to eject, already spend it and getting gum-ball.
		System.out.println("Sorry, you already turned the crank");
	}

	@Override
	public void turnCrank() {
		// No quarter, turning crank again does nothing
		System.out.println("Turning twice doesn't get you another gumball!");
	}

	@Override
	public void dispense() {
		// Give gum-ball to customer if we have gum-balls, subtract 1 from inventory, and move to NoQuarterState. If inventory reaches 0, move to SoldOutState
		
		if(!gumballMachine.isEmpty()){
			System.out.println("You get a gum-ball!");
			gumballMachine.releaseBall(); //updates number of gum-balls
			gumballMachine.setState(gumballMachine.getNoQuarterState());
		} else {
			System.out.println("Oops, out of gumballs!");
			gumballMachine.setState(gumballMachine.getSoldOutState());
		}
		
	}

	@Override
	public void refill(int count) {
		// Machine not empty; can't refill.
		System.out.println("Machine is not empty! Can't refill");
		
	}
	
	@Override
	public String toString(){
		return "sold a gum-ball, dispensing one gum-ball...";
	}

}
