package RemoteProxy;
/*
 * A state class (concrete implementation of the State superType). It encapsulates all information about a state. It implements behaviours that are appropriate for this state!
 * This state class will be responsible for the client's (The StateMachine) behaviour while it is in this state through delegation. (composition)
 * By behaviour, I mean the actions that it performs.
 * 
 * NOTE: In this example, this is the "Starting/Default" state.
 */

public class NoQuarterState implements State {
	
	//Mark as transient as we don't want to serialize state of these objects (set via constructor)
	transient GumballMachine gumballMachine;
	
	public NoQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void insertQuarter() {
		// Customer inserts a quarter into the machine. Update client's (state-machine's) state to "HasQuarterState".
		
		System.out.println("You inserted a quarter.");
		gumballMachine.setState(gumballMachine.getHasQuarterState());
	}

	@Override
	public void ejectQuarter() {
		// Can't eject quarter if haven't previously inserted quarter.
		
		System.out.println("You haven't inserted a quarter.");
	}

	@Override
	public void turnCrank() {
		// Nothing happens when turning crank without first inserting a quarter. 
		
		System.out.println("You turned, but there's no quarter.");
	}

	@Override
	public void dispense() {
		// Won't dispense anything without having first turned the crank (and therefore paying a quarter).
		
		System.out.println("You need to pay first.");
	}

	@Override
	public void refill(int count) {
		// Machine not empty; can't refill.
		System.out.println("Machine is not empty! Can't refill");
	}
	
	@Override
	public String toString(){
		return "waiting for quarter";
	}

}
