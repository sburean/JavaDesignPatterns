package RemoteProxy;
/*
 * A state class (concrete implementation of the State superType). It encapsulates all information about a state. It implements behaviours that are appropriate for this state!
 * This state class will be responsible for the client's (The StateMachine) behaviour while it is in this state through delegation. (composition)
 * By behaviour, I mean the actions that it performs.
 * 
 * NOTE: In this example, the SoldState represents the customer getting TWO gum-balls for their quarter. (We arrive here 10% of the time; as chance to win a free one is 10%)
 */

public class WinnerState implements State {

	GumballMachine gumballMachine;
	
	public WinnerState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}
	
	@Override
	public void insertQuarter() {
		// Currently dispensing gum-ball, wait before inserting quarter again.
		System.out.println("Please wait, we're already giving you a gum-ball");
	}

	@Override
	public void ejectQuarter() {
		// No quarter to eject, already spend it and getting gum-ball.
		System.out.println("Sorry, you already turned the crank");
	}

	@Override
	public void turnCrank() {
		// No quarter, turning crank again does nothing
		System.out.println("Turning twice doesn't get you another gumball!");
	}

	@Override
	public void dispense() {
		// Dispense one gum-all, update inventory, then dispense ANOTHER gum-ball and update inventory again; move to NoQuartersState if still have inventory.
		//NOTE: If we're in this state, we must have gum-balls available! Otherwise we'd have moved to SoldOutState before reaching this
		
		if(!gumballMachine.isEmpty()){
			System.out.println("WINNER: You get TWO gum-balls!");
			gumballMachine.releaseBall(); //updates number of gum-balls

			if(!gumballMachine.isEmpty()){
				
				//Incase customer won with only 1 ball left, have to check for emptiness again:
				gumballMachine.releaseBall(); 
				gumballMachine.setState(gumballMachine.getNoQuarterState());
				
			} else {
				
				System.out.println("Oops, ran out of gumballs! At least you still get one? :)");
				gumballMachine.setState(gumballMachine.getSoldOutState());
				
			}
			
		} else {
			System.out.println("Oops, out of gumballs!");
			gumballMachine.setState(gumballMachine.getSoldOutState());
		}
		
	}
	
	@Override
	public void refill(int count) {
		// Machine not empty; can't refill.
		System.out.println("Machine is not empty! Can't refill");
		
	}
	
	@Override
	public String toString(){
		return "customer won two gum balls, dispensing two gumballs...";
	}

}
