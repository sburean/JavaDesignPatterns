package RemoteProxy;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/*
 * The context (A state machine). In this example, it is the GumballMachine that accepts coins and gives gum-balls.
 * It is composed (strong HAS-A relationship) with ALL state objects used within the state-machine.
 * 
 * IMPORTANT:
 * - From the perspective of a client, it appears that this object is instantiated from different classes because it's behaviour changes.
 * 		-> BUT, this occurs because the context references a different state object to handle its behaviour, and the reference is updated after actions are performed.
 * 
 * NOTE: 
 * - Clients don't directly interact with states. States are used directly by the context to represent its internal state & behaviour, so the context
 * is responsible to oversee its state. (Obviously don't want client to change state of the context without the context's knowledge)
 * 
 * =====================================================================================================================================================================
 * 
 * This is the remote service (Real Subject). It exposes a remote interface that the client (GumballMonitor) will use 
 * 	to call methods on this remote object.
 * 
 * - Extends a java.rmi.server.UnicastRemoteObject to allow this class to act as a remote service. 
 * - Implements a remote interface (that it exposes) for client to use.
 * - Constructor of a remote service MUST throw a RemoteException because UnicastRemoteObject's constructor does.
 * -> Must register service with RMI registry by overriding java.rmi.Naming.rebind() method (In a launcher class of the remote service)
 */

public class GumballMachine extends UnicastRemoteObject implements RemoteInterface {

	//References to all states: (with getter methods, we minimize the dependency between concrete States themselves - they get new states to set from the gumBallMachine itself, instead of having to create them)
	private State soldOutState, noQuarterState, hasQuarterState, soldState, winnerState;
	
	private State currentState = soldOutState; //fail-safe initialization; start at "NoQuarterState" once constructor finishes
	private int count = 0;
	private String location;
	
	public GumballMachine(String location, int numberGumBalls) throws RemoteException {
		//Notice strong HAS-A association relationship between the state-machine and the state objects (ownership):
		
		/*NOTE: Classes extending UnicastRemoteObject MUST throw a RemoteException in their constructor 
			because the super-class's constructor does. (and is called automatically) */
		
		this.soldOutState = new SoldOutState(this);
		this.noQuarterState = new NoQuarterState(this);
		this.hasQuarterState = new HasQuarterState(this);
		this.soldState = new SoldState(this);
		this.winnerState = new WinnerState(this);
		this.location = location;
		
		this.count = numberGumBalls;
		if(count > 0){
			/* If the State-Machine was initialized with any gum-balls, set default state to "NoQuarterState */
			currentState = noQuarterState;
		}
	}
	
	/*
	 * Action methods: (All possible state transitions within the state-machine; as defined in the state interface)
	 * NOTE: See how easy actions are to implement with the StatePattern, simply delegate each action to the current state to handle.
	 */
	
	public void insertQuarter(){
		currentState.insertQuarter();
	}
	
	public void ejectQuarter(){
		currentState.ejectQuarter();
	}
	
	public void turnCrank(){
		currentState.turnCrank();
		currentState.dispense(); //NOTE: this is an INTERNAL action; simply called on a state object after the turn crank action (which changes state to either "SOLD" or "WINNER") 
	}
	
	public void setState(State state){
		/* 
		 * A setter method allows other objects (like other State objects) 
		 * to transition the State-Machine to a different state
		 */
		
		this.currentState = state;
	}
	
	public void releaseBall(){
		//A simple helper method that release a gum-ball and updates our inventory.
		if(count != 0){
			count--;
		}
	}
	
	public boolean isEmpty(){
		//Quick helper method to determine if this gumball machine has any gumballs left.
		return count == 0;
	}
	
	//More methods here: ( such as state getters, such that state objects can use them to transition the client [state-machine] to another state. )
	
	public void refill(int count){
		/*
		 * Alternative to having a refill() action method in the concrete states; call this method from one of the SoldOutState's action methods.
		 */
		this.count = count;
		this.currentState = noQuarterState;
	}
	
	public void setCount(int count){
		//To refill the gumBalls
		this.count = count;
	}
	
	public State getSoldOutState(){
		return this.soldOutState;
	}
	
	public State getNoQuarterState(){
		return this.noQuarterState;
	}
	
	public State getHasQuarterState(){
		return this.hasQuarterState;
	}
	
	public State getSoldState(){
		return this.soldState;
	}
	
	public State getWinnerState(){
		return this.winnerState;
	}
	
	public State getCurrentState(){
		return this.currentState;
	}
	
//	@Override
//	public String toString(){
//		if(count > 0){
//			return new String("\nMighty Bumball, Inc.\nJava-enabled Standing Gumball Mode #2004\nInventory: " + count + " gumballs\nMachine is waiting for quarter\n");
//		} else {
//			return new String("\nMighty Bumball, Inc.\nJava-enabled Standing Gumball Mode #2004\nInventory: " + count + " gumballs\nMachine is empty!\n");
//		}
//		
//	}
	
	/*REMOTE METHODS defined by the remote interface. Client calls these to get appropriate information for a report.
		(The shouldn't be used by the service class itself) */
	

	@Override
	public int getCount() throws RemoteException {
		return this.count;
	}

	@Override
	public String getLocation() throws RemoteException {
		return this.location;
	}

	@Override
	public State getState() throws RemoteException {
		return this.currentState;
	}
}
