package ProtectionProxy;

import java.lang.reflect.Proxy;

public class ProtectionProxyTestStub {

	public static void main(String[] args) {
		
		//Create people: (clients)
		PersonBean joe = new PersonBeanImpl();
		joe.setGender("male");
		joe.setName("Joe JavaBean");
		joe.setInterests("Java and stuff");
		joe.setHotOrNotRating(3);
		
		PersonBean jill = new PersonBeanImpl();
		jill.setGender("female");
		jill.setName("Jill something..");
		jill.setInterests("Turtles");
		jill.setHotOrNotRating(5);
		
		//Now call getters on clients:
		System.out.println("initial joe");
		System.out.println("name: " + joe.getName() + "\ngender: " + joe.getGender() 
				+ "\ninterests: " + joe.getInterests() + "\nrating: " + joe.getHotOrNotRating());
		System.out.println("----------");
		
		System.out.println("initial jill");
		System.out.println("name: " + jill.getName() + "\ngender: " + jill.getGender() 
				+ "\ninterests: " + jill.getInterests() + "\nrating: " + jill.getHotOrNotRating());
		System.out.println("----------");
		
		System.out.println("\n");
		
		//Create an OwnerProxy for joe: (doesn't allow setting own HotOrNot rating)
		PersonBean joeOwnerProxy = PersonBeanImpl.getProxy(joe, new OwnerInvocationHandler(joe));
		
		//Now call setters:
		try {
			joeOwnerProxy.setInterests("cats");
			joeOwnerProxy.setHotOrNotRating(9); //this would result in an average rating of 6 
			System.out.println("Joe rating: " + joeOwnerProxy.getHotOrNotRating());
		} catch (Exception e) {
			//But it WILL throw an exception, because RealSubject owner (joe) calling it on himself!
			System.err.println("ERROR: Cannot set own HotOrNot rating!");
		}
		
		//And check setters with getters:
		System.out.println("Joe interests: " + joeOwnerProxy.getInterests());
		System.out.println("Joe rating: " + joeOwnerProxy.getHotOrNotRating()); //should still be 3
		
		//Now create a NonOwnerProxy for joe: (only allows setting HotOrNot rating)
		PersonBean joeNonOwnerProxy = PersonBeanImpl.getProxy(joe, new NonOwnerInvocationHandler(joe)); 
		
		//And call setters again:
		try {
			joeNonOwnerProxy.setHotOrNotRating(9); //this should now result in an average of 6
			joeNonOwnerProxy.setInterests("nothing, i'm boring!");
		} catch (Exception e) {
			System.err.println("ERROR: Cannot set anything other than HotOrNot rating!");
		}
		
		//And check setters again with getters:
		System.out.println("Joe interests: " + joeOwnerProxy.getInterests());
		System.out.println("Joe rating: " + joeOwnerProxy.getHotOrNotRating()); //should now be 6
		
		if(Proxy.isProxyClass(joeOwnerProxy.getClass())){
			System.out.println("true");
		}
		
		
	}

}
