package ProtectionProxy;

/*
 * The object that does all of the dynamic proxy's real work. It is what the dynamic proxy calls to do real work.
 * Concrete InvocationHandlers MUST implement the java.lang.reflect.InvocationHandler interface 
 * 	& implement the invoke() method.  
 * 
 * This InvocationHandler is for a protection-proxy that prevents
 * 	non-owners from setting any other info other than Hot-Or-Not rating.
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NonOwnerInvocationHandler implements InvocationHandler {
	
	PersonBean person;
	
	public NonOwnerInvocationHandler(PersonBean person) {
		//Save a reference to a RealSubject, as per the java.lang.reflect proxy class diagram
		this.person = person;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

		/*
		 * We examine what method was called on the proxy, and make a decision based on the method's
		 * name and possibly arguments. (Use java.lang.reflect.Method package)
		 */
		
		try {
			
			if(method.getName().startsWith("get")){
				
				//client is accessing getters on the proxy; forward to RealSubject
				return method.invoke(person, args);
				
			} else if(method.getName().equals("setHotOrNotRating")) {
				
				//client is setting the hot-or-not rating for ANOTHER client; forward request to RealSubject
				return method.invoke(person, args);
				
			} else if(method.getName().startsWith("set")){
				
				//client is trying to set another client's information; throw exception. 
				throw new IllegalAccessException();
				
			}
			
		} catch (InvocationTargetException e) {
			// This happens if realSubject throws an exception
			e.printStackTrace();
		}
		
		//If any other method is called on the proxy, just return null. 
		return null;
	}

}
