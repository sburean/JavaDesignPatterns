package ProtectionProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/*
 * We need a protection proxy for this class because: (limit which clients may call certain methods)
 *
 * - Don't want users to be able to change other user's interests, or increasing their own Hot-Or-Not scores
 * 
 * Has a method that returns an appropriate proxy based on it's args: getProxy(..). 
 */

public class PersonBeanImpl implements PersonBean {
	
	//Instance variables
	private String name;
	private String gender;
	private String interests;
	private int rating;
	private int ratingCount = 0;

	//Getters:
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getGender() {
		return gender;
	}

	@Override
	public String getInterests() {
		return interests;
	}

	@Override
	public int getHotOrNotRating() {
		//returns average rating: rating/ratingCount
		return ratingCount == 0 ? 0 : (rating/ratingCount); 
	}
	
	public static PersonBean getProxy(final PersonBean realSubject, final InvocationHandler handler){
		
		/*
		 * A class to create our proxy object.
		 * NOTE: Since proxy classes do not have accessible names, they cannot have constructors. 
		 * 			So they must instead be created by factories. (Like this; via a factory method)
		 */
		
		/* KEY:
		 * This method takes an InvocationHandler and a RealSubject.
		 * Returns a java.lang.reflect.Proxy for the RealSubject that uses the handler.
		 */
		
		return (PersonBean) Proxy.newProxyInstance(
				realSubject.getClass().getClassLoader(),
				realSubject.getClass().getInterfaces(),
				handler); //NOTE: handler has to be a concrete InvocationHandler class

	}

	//Setters:
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public void setInterests(String interests) {
		/*
		 * We don't want users to be able to change other's interests. 
		 */
		this.interests = interests;
	}

	@Override
	public void setHotOrNotRating(int rating) {
		/*
		 * We don't want users to increase their own rating.
		 */
		
		//increments rating count and appends value to total rating. 
		this.rating += rating;
		ratingCount++;
	}

}
