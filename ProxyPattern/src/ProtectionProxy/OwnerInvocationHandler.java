package ProtectionProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
 * The object that does all of the dynamic proxy's real work. It is what the dynamic proxy calls to do real work.
 * Concrete InvocationHandlers MUST implement the java.lang.reflect.InvocationHandler interface 
 * 	& implement the invoke() method.  
 * 
 * This InvocationHandler is for a protection-proxy that prevents 
 * 	owners from incrementing their own hot-or-not rating. (via the setHotOrNotRating() method)
 */

public class OwnerInvocationHandler implements InvocationHandler {

	PersonBean person;
	
	public OwnerInvocationHandler(PersonBean person) {
		//Save a reference to a RealSubject, as per the java.lang.reflect proxy class diagram
		this.person = person;
	}
	
	//The invoke method that gets called each time a method is invoked on the proxy.
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		/*
		 * We examine what method was called on the proxy, and make a decision based on the method's
		 * name and possibly arguments. (Use java.lang.reflect.Method package)
		 */
		
		try {
			
			if(method.getName().startsWith("get")){
				
				//a getter was invoked on the proxy; forward request to realSubject
				return method.invoke(person, args);
				
			} else if(method.getName().equals("setHotOrNotRating")){
				
				//setting the hot-or-not rating, only want NON-OWNERS to access this, NOT the owner; throw exception.
				throw new IllegalAccessException();
				
			} else if(method.getName().startsWith("set")){
				
				//other setters, forward request to realSubject
				return method.invoke(person, args);
				
			}
			
		} catch (InvocationTargetException e) {
			// This happens if realSubject throws an exception
			e.printStackTrace();
		}
		
		//If any other method is called on the proxy, just return null. 
		return null;
	}

}
