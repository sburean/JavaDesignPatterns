/*
 * A concrete pizza product belonging to the types of pizzas category. It has a supertype of pizza.
 * Objects of this class will be instantiated by the factory object's factory method. 
 */

public class CheesePizza implements Pizza {
	
	PizzaType pizzaType;
	
	public CheesePizza() {
		this.pizzaType = PizzaType.Cheese;
	}

	@Override
	public void description() {
		System.out.println("Cheese Pizza! A little bit plain..");
	}

}
