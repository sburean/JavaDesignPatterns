/*
 * This is a product super-type. All concrete products (pizzas) must have a super-type so they may be -
 * - decoupled from the objects that use them through the use of the factory object.
 * 
 * The SimpleFactoryMethod can only create objects of ONE category (ie: types of pizzas), -
 * - belonging to ONE ProductSuperType. (pizza)
 */

public interface Pizza {

	void description();
	
}
