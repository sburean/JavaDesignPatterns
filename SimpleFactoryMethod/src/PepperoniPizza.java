/*
 * A concrete pizza product belonging to the types of pizzas category. It has a supertype of pizza.
 * Objects of this class will be instantiated by the factory object's factory method. 
 */

public class PepperoniPizza implements Pizza {
	
	PizzaType pizzaType;
	
	public PepperoniPizza() {
		this.pizzaType = PizzaType.Pepperoni;
	}

	@Override
	public void description() {
		System.out.println("Pepperoni Pizza, Meh.");
	}

}
