/*
 * A concrete pizza product belonging to the types of pizzas category. It has a supertype of pizza.
 * Objects of this class will be instantiated by the factory object's factory method. 
 */

public class VeggiePizza implements Pizza {
	
	PizzaType pizzaType;
	
	public VeggiePizza() {
		this.pizzaType = PizzaType.Veggie;
	}

	@Override
	public void description() {
		System.out.println("A veggie pizza, yuck!");
	}

}
