/*
 * A concrete pizza product belonging to the types of pizzas category. It has a supertype of pizza.
 * Objects of this class will be instantiated by the factory object's factory method. 
 */

public class HawaiianPizza implements Pizza {
	
	PizzaType pizzaType;
	
	public HawaiianPizza() {
		this.pizzaType = PizzaType.Hawaiian;
	}

	@Override
	public void description() {
		System.out.println("Hawaiian Pizza, Yumm!");
	}

}
