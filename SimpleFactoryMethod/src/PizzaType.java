/*
 * To differentiate between product variations from one category (different pizza types) belonging to the Pizza super-type. 
 */

public enum PizzaType {
	Cheese, Veggie, Hawaiian, Pepperoni
}
