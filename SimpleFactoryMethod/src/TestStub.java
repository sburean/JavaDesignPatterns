/*
 * This test stub simply creates a factory object, and calls upon it to create one of each concrete product of one category. (types of pizzas)
 * 
 * Concrete product creation is decoupled from this stub, we're declaring our products by their super-type. 
 * (Only the factory knows about the concrete product type)
 */

public class TestStub {

	public static void main(String[] args) {
		
		SimplePizzaFactory pizzaFactory = new SimplePizzaFactory(); //Our SimplePizzaFactory
		
		Pizza pizzaOne = pizzaFactory.createPizza(PizzaType.Cheese); // A cheese pizza
		Pizza pizzaTwo = pizzaFactory.createPizza(PizzaType.Veggie); // A veggie pizza
		Pizza pizzaThree = pizzaFactory.createPizza(PizzaType.Hawaiian); // A hawaiian pizza
		Pizza pizzaFour = pizzaFactory.createPizza(PizzaType.Pepperoni); // A pepperoni pizza
		
		pizzaOne.description();
		pizzaTwo.description();
		pizzaThree.description();
		pizzaFour.description();
		
	}

}
