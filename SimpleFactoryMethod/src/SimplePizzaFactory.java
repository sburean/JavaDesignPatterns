/*
 * This is the concrete factory object. It has a factory method to create various products belonging to one category of one super-type. 
 * Any other methods aside simply exist to operate on the products produced by the factory method. 
 * Used to encapsulate object creation with loose coupling. 
 * 
 * This factory (SimpleFactoryMethod) is simply an object composed through association with the product it instantiates. 
 * 
 * NOTES:
 * - If product variations exist with a category, need to parameterize factory method and use a switch statement (as shown here in example).
 * - Factory (or Factory Method) may be static, remove need to instantiate but won't be open for modification. (Can't extend to change behaviour)
 * - Decouples implementation of a product from its use. 
 * ( Objects that use products, like a testStub, will refer to them by their super-type. It won't know the concrete product type. )
 */

public class SimplePizzaFactory {

	public Pizza createPizza(PizzaType pizzaType){
		//The factory method
		
		switch(pizzaType){
			case Veggie:
				return new VeggiePizza();
			case Cheese:
				return new CheesePizza();
			case Pepperoni:
				return new PepperoniPizza();
			case Hawaiian:
				return new HawaiianPizza();
			default:
				throw new AssertionError("ERROR: Reached default case in SimplePizzaFactory:createPizza(type)");
				
		}

	}
	
}
