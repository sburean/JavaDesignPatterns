/*
 * To differentiate between variations within categories of "Product1SuperType" 
 */

public enum Variation {
	A, B, C
}
