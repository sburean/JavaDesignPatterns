/*
 * The product super-type. The factory method can create multiple product categories -
 * - belonging to one super-type; this one. (with many variations within each category) 
 * 
 * NOTE: Category variations must be the same across multiple categories if want to use one parameterized factory method. (keeping it clean)
 * 			-> In this case, can make the factory method return null on default switch case and simply check for that in the client. 
 * 
 * All products that are to be created by the factory pattern must have a super-type. 
 */

public interface Product1SuperType {
	
	public void method();

}
