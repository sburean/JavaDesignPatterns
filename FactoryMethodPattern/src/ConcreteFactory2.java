/*
 * A concrete factory. It encapsulates all knowledge about, and is responsible for creating -
 * - all variations of concrete products belonging to category 2, of Product1SuperType.
 * 
 *  Provides specific implementation to the factory method such that it produces the above.
 * 
 */
public class ConcreteFactory2 extends FactorySuperType {

	@Override
	protected Product1SuperType CreateObject(Variation objVariation) {
		//Specific implementation of the abstract factory method; parameterized to account for variations within a product category. 
		
		switch(objVariation){
			case A:
				return new Category2Product1_A(Variation.A);
			case B:
				return new Category2Product1_B(Variation.B);
			case C:
				return new Category2Product1_C(Variation.C);
			default:
				throw new IllegalArgumentException("Error: No product available in category 2 for specified variation!");
				
		}
	}

}
