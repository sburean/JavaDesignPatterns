/*
 * A concrete product belonging to category1 of Product1SuperType.
 * It has multiple variations as defined by the Enum: "Variation" 
 */

public class Category1Product1_A implements Product1SuperType {

	Variation categoryVariation;
	
	public Category1Product1_A(Variation variation) {
		this.categoryVariation = variation;
	}

	@Override
	public void method() {
		System.out.println("Category1Product1_A");
	}

}
