/*
 * A concrete product belonging to category1 of Product1SuperType.
 * It has multiple variations as defined by the Enum: "Variation" 
 */

public class Category1Product1_C implements Product1SuperType {

	Variation categoryVariation;
	
	public Category1Product1_C(Variation variation) {
		this.categoryVariation = variation;
	}

	@Override
	public void method() {
		System.out.println("Category1Product1_C");
	}

}
