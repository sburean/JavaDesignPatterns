/*
 * The "client". It is decoupled from the concrete products; only refers to factories and products by their super-type.
 * The Factory Method "Decouples concrete products from objects that use them". 
 * -> See that it only relies about super-types. 
 * 
 * FactoryMethod creates objects through inheritance. The client instantiates a concrete factory, -
 * - which is then responsible for creating concrete objects of one category. 
 * ( -> By inheriting the factory method from the abstract factory and using a specific implementation via the concrete factory) 
 * 
 */

public class TestStub {

	public static void main(String[] args) {
		//Create concrete factories (each inherits factory method; and have different specific implementations)
		FactorySuperType concreteFactory1 = new ConcreteFactory1();
		FactorySuperType concreteFactory2 = new ConcreteFactory2();
		
		/*
		 *  Create Products
		 * 	NOTE: If accidentally use concreteFactory2 to create objects of category1, there will be NO compile error. 
		 * 	HOWEVER, will reach default in switch and will get assertion error.
		 */
		
		Product1SuperType product1 = concreteFactory1.CreateObject(Variation.A); //product1, category1, variation A
		Product1SuperType product2 = concreteFactory1.CreateObject(Variation.B); //product1, category1, variation B
		Product1SuperType product3 = concreteFactory1.CreateObject(Variation.C); //product1, category1, variation C
		
		Product1SuperType product4 = concreteFactory2.CreateObject(Variation.A); //product1, category2, variation A
		Product1SuperType product5 = concreteFactory2.CreateObject(Variation.B); //product1, category2, variation B
		Product1SuperType product6 = concreteFactory2.CreateObject(Variation.C); //product1, category2, variation C
		
		//Now call each product's method to see its description. 
		product1.method();
		product2.method();
		product3.method();
		product4.method();
		product5.method();
		product6.method();

	}

}
