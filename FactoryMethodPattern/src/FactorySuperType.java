/*
 * The factory super-type for creating products by letting subclasses decide -
 * - which concrete products to instantiate. The concrete products created are decided purely -
 * - by choice of concrete factory used by the client. 
 * 
 * Contains an abstract factory method to be implemented by concrete factories.
 * (Parameterized if products vary within each category) 
 * Other methods exist purely to operate on the created products.
 * 
 * IMPORTANT NOTE: There is a ratio of 1:1 between ConcreteFactories and ProductCategories. 
 * 					 -> Each ConcreteFactory is responsible for one product category.
 * 
 * The FactoryMethod pattern is like the TemplateMethod pattern, but for object creation/instantiation instead of algorithms. 
 * 	-> It provides generic behaviour with abstract factory method(s) for object creation, and allows concrete subclasses to provide specific implementations.
 * 	(and hence: it allows subclasses to decide which class to instantiate)
 */

public abstract class FactorySuperType {

	abstract protected Product1SuperType CreateObject(Variation objVariation); //protected such that only accessible via same package, or outside of package through inheritance - same for concrete subclasses?
	
	//Other methods here as needed to operate on any created Product1SuperType. 
	
}
