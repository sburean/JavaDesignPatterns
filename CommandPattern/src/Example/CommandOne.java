package Example;

public class CommandOne implements Command {

	/* Command object; encapsulates a request as an object by binding together a set of actions on a specific receiver */
	
	ReceiverA receiverA;
	
	public CommandOne(ReceiverA receiverA){
		/* This command object will perform actions from receiverA */
		this.receiverA = receiverA;
	}
	
	@Override
	public void execute() {
		/* Can contain as many actions from receiverA as needed to get a job done (in whatever order is needed) */
		receiverA.stepA();
		receiverA.stepB();
	}

}
