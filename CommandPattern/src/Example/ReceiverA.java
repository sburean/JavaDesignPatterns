package Example; 

public class ReceiverA {

	/* One receiver; performs a specific action -> Can be 3rd party objects */
	
	public void stepA(){
		System.out.println("ReceiverA is performing stepA.");
	}
	
	public void stepB(){
		System.out.println("ReceiverA is performing stepB.");
	}
	
}
