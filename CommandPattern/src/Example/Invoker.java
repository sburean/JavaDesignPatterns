package Example; 

public class Invoker {

	/* Invoker; stores and executes commands */
	
	/*
	 * Invoker is key for supporting:
	 * - Undo
	 * - MultiUndo
	 * - Queuing
	 * - Logging
	 */
	
	Command slot; // 'slot' for storing command object(s) 
	
	public Invoker(){
		/* initialize command slot with a null object */
		this.slot = new NullObject();
	}
	
	public void setCommand(Command cmd){
		/* store specified command object */
		this.slot = cmd;
	}
	
	public void doAction(){
		/* Invoke method on command object(s) to get an action done */
		slot.execute();
	}
}
