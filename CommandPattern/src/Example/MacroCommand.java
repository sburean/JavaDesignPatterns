package Example; 

public class MacroCommand implements Command {

	/* Command object; encapsulates a bunch of requests together to be performed sequentially */
	
	Command[] commands;
	
	public MacroCommand(Command[] commands) {
		/* Constructor initializes Macro Command objects with an array of commands */
		this.commands = commands;
	}

	@Override
	public void execute() {
		for(Command cmd : commands){
			/* For each command in the commands array, invoke the execute method to perform their action */
			cmd.execute();
		}
	}

}
