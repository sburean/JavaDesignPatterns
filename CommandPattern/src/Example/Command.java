package Example;

public interface Command {

	public void execute(); //command objects expose this method
//	public void undo(); 
	
	/*
	 * If supported, an undo operation will do the opposite of the LAST execute operation.
	 * The INVOKER needs to support the undo operation by keeping track of the LAST EXECUTED COMMAND,
	 * 		and in the "undo action" method, it will simply call undo on that stored command. 
	 * -> Each command object will properly implement it's own undo functionality. 
	 * 
	 * NOTE: If receiver's state changes as a result of the operation, the command object action on that 
	 * 			receiver will need to keep track of its (receiver's) state to properly revert.
	 */
	
	/*
	 * Note, for multi-undos... The invoker needs to keep a STACK of previously performed commands, and
	 * 	simply pop one off the stack per undo action to revert the action.
	 */
	
}
