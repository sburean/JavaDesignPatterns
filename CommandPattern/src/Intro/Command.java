package Intro;
/*
 * The command interface. All command objects need to implement this so they share an "execute()" method.
 * It will be called by the invoker when instructed to such that a command's actions are carried out. 
 * 
 * The "undo()" method, if supported, will perform the opposite action of the last execute() method.
 */

public interface Command {

	public void execute();
	public void undo();
	
}