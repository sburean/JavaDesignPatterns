package Intro;
/*
 * The invoker class. It contains "slots" to hold command object(s). [Data fields, data structures, etc..]
 * -> Should contain a setter method for command object(s). This will be called by clients to store commands.
 * -> Should also contain a method to execute these stored commands. 
 * 		(Also to be called by clients whenever an action is required) 
 * 
 * For the invoker to support the undo() functionality:
 * 	- Need to have a Command member-variable to store last executed command via execute() method
 * 	- This member-varaible is updated each time a new command is executed
 * 		(Remember that it is initialized with a NoCommand in the constructor! -> avoids NPE)
 * 	- When an undo action is requested, simply call undo on this member-variable. 
 */

public class RemoteControl {
	
	private static final int COMMAND_SLOT_SIZE = 7;
	
	private Command[] onCommands;
	private Command[] offCommands;
	private Command lastCommand; //for undo functionality
	
	public RemoteControl(){
		//constructor initializes command slots with appropriate size
		onCommands = new Command[COMMAND_SLOT_SIZE];
		offCommands = new Command[COMMAND_SLOT_SIZE];
		
		//create an empty placeholder command
		NoCommand noCommand = new NoCommand();
		
		for (int i = 0; i < COMMAND_SLOT_SIZE; i++) {
			//now use placeholder command to "zero-out" arrays and avoid NPE
			onCommands[i] = offCommands[i] = noCommand;
		}
		
		lastCommand = noCommand; //initially have nothing to undo 
	}
	
	public void setCommand(int slot, Command onCommand, Command offCommand){
		//This is the method to store command(s).
		onCommands[slot] = onCommand;
		offCommands[slot] = offCommand;
	}
	
	public void onButtonWasPushed(int slot){
		//This is the method to execute the stored onCommand(s).  We know all commands have an execute() method from the interface.
		onCommands[slot].execute();
		lastCommand = onCommands[slot]; // record it as the last action performed
	}
	
	public void offButtonWasPushed(int slot){
		//This is the method to execute the stored offCommand(s).  We know all commands have an execute() method from the interface.
		offCommands[slot].execute();
		lastCommand = offCommands[slot]; // record it as the last action performed
	}
	
	public void undoButtonWasPushed(){
		//This method is used to undo the last action executed. Simply call the lastCommand's undo method.
		lastCommand.undo();
	}
	
	@Override
	public String toString(){
		//Overriding the toString method to print the command that was executed. (Testing purposes)
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("------ Remote Control -------\n");
		for (int i = 0; i < COMMAND_SLOT_SIZE; i++) {
			stringBuffer.append("[slot " + i + "] " + onCommands[i].getClass().getName()+ " " + offCommands[i].getClass().getName() + "\n");
		}
		stringBuffer.append("------ Remote Control -------\n");
		return stringBuffer.toString();
	}
	
}
