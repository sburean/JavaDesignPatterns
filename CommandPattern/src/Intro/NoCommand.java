package Intro;
/*
 * An empty command to-be-used as a placeholder when a slot has no valid command set. 
 * (So slots don't stay null and produce a NPE when invoked)
 */
public class NoCommand implements Command {

	@Override
	public void execute() {
		System.out.println("Empty Command Slot! - nothing to execute");
	}

	@Override
	public void undo() {
		System.out.println("Empty Command Slot! - nothing to undo");
	}

}
