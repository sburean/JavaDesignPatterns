package Intro;
/*
 * A command object. It must implement the Command interface. 
 * It encapsulates instructions for an appropriate receiver to carry out some action.  
 * 
 * The execute() method from the interface will contain instructions -
 * - for the appropriate receiver to carry out this action. 
 * 
 *  * The undo() method on the other hand will contain instructions -
 * - to revert the actions/change in state that the execute() method performed. 
 */

public class GarageDoorUpCommand implements Command{

	GarageDoor garageDoor;
	
	public GarageDoorUpCommand(GarageDoor garageDoor) {
		this.garageDoor = garageDoor;
	}
	
	@Override
	public void execute() {
		garageDoor.up();
	}

	@Override
	public void undo() {
		garageDoor.down();
	}
	
}
