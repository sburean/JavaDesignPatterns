package Intro;
/*
 * A command object. It must implement the Command interface. 
 * It encapsulates instructions for an appropriate receiver to carry out some action. 
 * 
 * The macro command is a command made up of multiple other commands. 
 * It may accept an array of commands. It simply executes/undos commands within the array sequentially.
 *  
 * (Actual command logic already taken care of in the individual command objects)
 */

public class MacroCommand implements Command{

	Command[] commands;
	
	public MacroCommand(Command[] commands) {
		this.commands = commands;
	}
	
	@Override
	public void execute() {
		for (int i = 0; i < commands.length; i++) {
			commands[i].execute();
		}
	}
	
	@Override
	public void undo() {
		for (int i = 0; i < commands.length; i++) {
			commands[i].undo();
		}
	}

}
