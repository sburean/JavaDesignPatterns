package Intro;
/*
 * A receiver. It has methods that perform some actions. 
 * This class may be provided by third party corporations. 
 */

public class Stereo {

	String location;
	
	public Stereo(String location){
		this.location = location;
	}
	
	public void on(){
		System.out.println(location + " stereo is on.");
	}
	
	public void off(){
		System.out.println(location + " stereo is off.");
	}
	
	public void setCD(){
		System.out.println(location + " stereo CD is set.");
	}
	
	public void setDVD(){
		System.out.println(location + " stereo DVD is set.");
	}
	
	public void setRadio(){
		System.out.println(location + " stereo radio is on.");
	}
	
	public void setVolume(int vol){
		System.out.println(location + " stereo volume is set to: " + vol + ".");
	}
	
}
