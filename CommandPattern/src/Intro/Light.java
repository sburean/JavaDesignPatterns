package Intro;
/*
 * A receiver. It has methods that perform some actions. 
 * This class may be provided by third party corporations. 
 */

public class Light {
	
	String location;
	
	public Light(String location){
		this.location = location;
	}
	
	public void on(){
		System.out.println(location + " light is on.");
	}
	
	public void off(){
		System.out.println(location + " light is off.");
	}

}
