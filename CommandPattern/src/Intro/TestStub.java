package Intro;
/*
 * This is the test stub for the command pattern. (The "client" in terms of command pattern components)
 * It creates an instance of the invoker.
 * It also creates instances of various receivers along with some commands to perform some actions. 
 * 
 * Commands are then assigned to the invoker, and after some time they are executed. 
 */

public class TestStub {

	public static void main(String[] args) {
		
		/* Instantiate invoker */
		RemoteControl remoteControl = new RemoteControl(); 
		
		/* Create receivers */
		Light livingRoomLight = new Light("Living Room");
		Light kitchenLight = new Light("Kitchen");
		GarageDoor garageDoor = new GarageDoor();
		Stereo stereo = new Stereo("Living Room");
		
		/* Create command objects; NOTE how they are bound to appropriate receivers at this point */
		LightOnCommand livRoomLightOnCommand = new LightOnCommand(livingRoomLight);
		LightOffCommand livRoomLightOffCommand = new LightOffCommand(livingRoomLight);
		
		LightOnCommand kitchenLightOnCommand = new LightOnCommand(kitchenLight);
		LightOffCommand kitchenLightOffCommand = new LightOffCommand(kitchenLight);
		
		GarageDoorUpCommand garageDoorUpCommand = new GarageDoorUpCommand(garageDoor);
		GarageDoorDownCommand garageDoorDownCommand = new GarageDoorDownCommand(garageDoor);
		
		StereoOnWithCDCommand stereoOnWithCDCommand = new StereoOnWithCDCommand(stereo);
		StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);
		
		/* Now set the commands into the invoker */
		remoteControl.setCommand(0, livRoomLightOnCommand, livRoomLightOffCommand);
		remoteControl.setCommand(1, kitchenLightOnCommand, kitchenLightOffCommand);
		remoteControl.setCommand(2, garageDoorUpCommand, garageDoorDownCommand);
		remoteControl.setCommand(3, stereoOnWithCDCommand, stereoOffCommand);
		
		System.out.println(remoteControl.toString()); //Print all assigned commands
		
		/* Now execute each command */
		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
		
		System.out.println("-----------------------");
		
		remoteControl.onButtonWasPushed(1);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
		remoteControl.offButtonWasPushed(1);
		
		System.out.println("-----------------------");
		
		remoteControl.onButtonWasPushed(2);
		remoteControl.offButtonWasPushed(2);
		
		System.out.println("-----------------------");
		
		remoteControl.onButtonWasPushed(3);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
		remoteControl.offButtonWasPushed(3);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
		
		/* Create a macro-command */
		
		Command[] everythingONcommand = 
			{livRoomLightOnCommand, kitchenLightOnCommand, garageDoorUpCommand, stereoOnWithCDCommand};
		
		Command[] everythingOFFcommand = 
			{livRoomLightOffCommand, kitchenLightOffCommand, garageDoorDownCommand, stereoOffCommand};
		
		MacroCommand macroON = new MacroCommand(everythingONcommand);
		MacroCommand macroOFF = new MacroCommand(everythingOFFcommand);
		
		remoteControl.setCommand(4, macroON, macroOFF);
		
		System.out.println("-----------MACRO-ON------------");
		remoteControl.onButtonWasPushed(4);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
		
		System.out.println("-----------MACRO-OFF------------");
		remoteControl.offButtonWasPushed(4);
		System.out.println("...undoing...");
		remoteControl.undoButtonWasPushed();
	}

}
