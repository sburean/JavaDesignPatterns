package Intro;
/*
 * A command object. It must implement the Command interface. 
 * It encapsulates instructions for an appropriate receiver to carry out some action.  
 * 
 * The execute() method from the interface will contain instructions -
 * - for the appropriate receiver to carry out this action.  
 * 
 * The undo() method on the other hand will contain instructions -
 * - to revert the actions/change in state that the execute() method performed. 
 */

public class LightOffCommand implements Command {

	Light light;
	
	public LightOffCommand(Light light){
		/* 
		 * The constructor is passed an instance of the receiver that this command is going to control.
		 * When the execute() method is called, this is going to be the receiver of the request. 
		 * 
		 * ("The specific light that we're turning off; living room light, or garage light, etc..") 
		 */ 
		this.light = light;
	}
	
	@Override
	public void execute() {
		//This action will be to turn off a light. (Call methods on the receiving object)
		light.off();
	}

	@Override
	public void undo() {
		//Will undo turning on a light 
		light.on();
	}

}
