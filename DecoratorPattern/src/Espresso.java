/*
 * A concrete component implementation. 
 * Provides basic implementation of the super-type. 
 */

public class Espresso extends Beverage {

	public Espresso(){
		this.description = "Espresso"; //description inherited from superclass "Beverage"
	}
	
	@Override
	public double getCost() {
		return 1.99;
	}

}
