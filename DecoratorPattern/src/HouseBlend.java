/*
 * A concrete component implementation. 
 * Provides basic implementation of the super-type. 
 */

public class HouseBlend extends Beverage {

	public HouseBlend() {
		this.description = "House Blend COffee";
	}

	@Override
	public double getCost() {
		return 0.89;
	}

}
