/*
 * A concrete decorator. It has the same super-type as the object it's decorating because the Decorator superclass extends the component super-type. 
 * This extends the base decorator functionality and adds own functionality to the component's default behaviour.
 * ( Either BEFORE or AFTER delegating the original behaviour to the object its decorating, via a call to super() )
 */

public class Mocha extends CondimentDecorator {
	
	// Data filed that holds super-type being decorated. (Declared in base decorator, and assigned via super() in constructor)

	public Mocha(Beverage beverage) {
		
		/* 
		 * The constructor simply calls super() to associate with the object being decorated.
		 * NOTE: Each individual concrete decorator can associate with the object it's decorating by declaring it as a data field and assigning it in the constructor. 
		 * (That requires a lot more coding and maintenance for potential future changes.)
		 */
		
		super(beverage);
	}

	public String getDescription() {
		/* Delegate original behaviour to the associated object, and then add new description. NOT Overridden, simply an implementation of an abstract method. */
		return beverage.getDescription() + ", Mocha";
	}
	
	@Override
	public double getCost(){
		/* Modify getCost() method of the superclass. 
		 * Delegate original behaviour to the associated object, and then add new cost to total */
		return super.getCost() + 0.20;
	}

}
