/*
 * The concrete super-type. It defines the default behaviour. (methods to be implemented) 
 */

public abstract class Beverage {

	String description = "Unknown Beverage";
	
	public String getDescription(){
		return description;
	}
	
	public abstract double getCost();
	
}
