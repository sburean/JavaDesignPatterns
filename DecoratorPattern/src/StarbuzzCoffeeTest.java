/*
 * This is a test stub for the Decorator Pattern. 
 * We create a concrete component and then decorate it with various concrete decorators. 
 * NOTE: How the cost and description behaviours are delegated to the original components and extra functionality is appended. 
 */
public class StarbuzzCoffeeTest {

	public static void main(String[] args) {
		
		Beverage beverage = new Espresso();
		System.out.println(beverage.getDescription() + " = $" + beverage.getCost());
		
		Beverage beverage2 = new DarkRoast();
		beverage2 = new Mocha(beverage2); //decorate DarkRoast with Mocha.
		beverage2 = new Mocha(beverage2); //and again
		beverage2 = new Whip(beverage2); //now decorate it with whip
		System.out.println(beverage2.getDescription() + " = $" + beverage2.getCost());
		
		Beverage beverage3 = new HouseBlend();
		beverage3 = new Soy(beverage3);
		beverage3 = new Mocha(beverage3);
		beverage3 = new Whip(beverage3);
		System.out.println(beverage3.getDescription() + " = $" + beverage3.getCost());
		
	}

}
