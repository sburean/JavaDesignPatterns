/*
 * A concrete decorator. It has the same super-type as the object it's decorating because the Decorator superclass extends the component super-type. 
 * This extends the base decorator functionality and adds own functionality to the component's default behaviour.
 * ( Either BEFORE or AFTER delegating the original behaviour to the object its decorating, via a call to super() )
 */

public class Soy extends CondimentDecorator {

	public Soy(Beverage beverage) {
		super(beverage);
	}

	public String getDescription() {
		/* Delegate original behaviour to the associated object, and then add new description. NOT Overridden, simply an implementation of an abstract method. */
		return beverage.getDescription() + ", Soy";
	}
	
	@Override
	public double getCost(){
		/* Modify getCost() method of the superclass. 
		 * Delegate original behaviour to the associated object, and then add new cost to total */
		return super.getCost() + 0.15;
	}

}
