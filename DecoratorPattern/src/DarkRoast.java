/*
 * A concrete component implementation. 
 * Provides basic implementation of the super-type. 
 */

public class DarkRoast extends Beverage {

	public DarkRoast() {
		this.description = "Dark Roast";
	}

	@Override
	public double getCost() {
		return 0.99;
	}

}
