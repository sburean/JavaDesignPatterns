/*
 * The decorator superclass. It is of the same super-type as the object it's decorating (Wrapping) by extending another class or implementing an interface. 
 * Due to Polymorphism, all concrete decorators extending this class may be used interchangeably -
 * - with the concrete components (Because the decorator is of the same super-type).
 *  All decorators are associated with the object they're decorating by using reference of super-type to various objects (program to an interface!)
 */

public abstract class CondimentDecorator extends Beverage {

	protected Beverage beverage;//NOTE: This can be left to each individual concrete decorator, however that's more code to type. 
	
	protected CondimentDecorator(Beverage beverage) {
		
		/*
		 * Decorator constructor take an argument of super-type for the components it's going to decorate, 
		 * and assigns it to the protected data field. 
		 */
		
		this.beverage = beverage;// NOTE: "this" refers to the non-abstract class that is instantiated, since abstract classes cannot be instantiated. 
	}
	
	public double getCost() {
		
		/*
		 * Specific implementation of this method -> Simply delegate default behaviour to the original object. 
		 */
		
		return beverage.getCost();
	}
	
	/* Declaring this requires all decorators re-implement the getDescription() method stated in the -
	 * - beverage superclass because we want to append the items decorating the beverage to the description
	 * (otherwise concrete decorators wouldn't have to implement the getDescription() method) */
	public abstract String getDescription();
}
