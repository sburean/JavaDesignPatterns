/*
 * A concrete component implementation. 
 * Provides basic implementation of the super-type. 
 */

public class Decaf extends Beverage {

	public Decaf() {
		this.description = "Decaf";
	}

	@Override
	public double getCost() {
		return 1.05;
	}

}
