/*
 * The class that we wish to only have ONE instance of (and want to enforce this). 
 * Make the constructor private, and declare a static variable of the class type to hold an instance of itself.
 * Declare a public static "getInstance()" method. -> Will instantiate class and/or return the -
 * - instance stored in the static variable.
 * 
 * IMPORTANT: 
 * 	Need to account for multithreading issues such as two threads accessing -
 * 	- getInstance() simultaneously to create two instances! Solve using the following methods:
 * 		1: Synchronized Static Method 
 * 			-> Synchronize whole method, only one thread will be allowed for same monitor object (MyClass.class).
 * 			-> Large overhead; usually unnecessary because only really needed first time singleton instantiated.
 * 		2: EagerInstantiation over LazyInstantiation 
 * 			-> LazyInstantiation = instantiating singleton within the getInstance() method when called. (multithreading issues)
 * 			-> EagerInstantiation = instantiate singleton when declaring it as a static data field. 
 * 			   JVM performs this instantiation before any threads access the instance.
 * 		3: "Double-Checked Locking" (https://en.wikipedia.org/wiki/Double-checked_locking)
 * 			-> Check the lock condition before actually acquiring the lock. (Improves performance)
 * 			-> Once have the lock, check the lock condition again in case another thread got lock and instantiated first
 * 			-> If lock condition is true again, perform instantiation. 
 * 			-> PERFORMANCE: Static member variable now needs to be volatile, and use local variable for checking conditions
 * 							(SEE LINK, note use of "result" local variable)
 * 
 * EXAMPLE:
 * Don't want to fill boiler when already full, or boil an empty boiler, etc... Only want one instance of this class!
 */

public class ChocolateBoiler {

	private boolean empty, boiled;
	private static volatile ChocolateBoiler chocolateBoilerInstance;
	
	private ChocolateBoiler() {
		//Private constructor
		empty = true;
		boiled = false;
	}
	
	public static ChocolateBoiler getInstance(){
		//See the different multithreading solutions; using Double-Checked Locking here.
		
		/*
		 * NOTE: Unnecessary local variable; In-case singleton instance already initialized.
		 * Leads to volatile instance only being accessed once. (In the line below)
		 * --> Helps improve performance up to 25%.
		 */
		ChocolateBoiler tmpInstance = chocolateBoilerInstance; 
		
		if(tmpInstance == null){
			//Singleton not yet initialized
			
			synchronized(ChocolateBoiler.class){
				//synchronize on the class object of the class the methods belong to 
				
				/* CHECK AGAIN in-case another thread got the lock first and instantiated the singleton first */
				tmpInstance = chocolateBoilerInstance; 
				
				if(tmpInstance == null){
					//Singleton still not instantiated; instantiate it:
					chocolateBoilerInstance = tmpInstance = new ChocolateBoiler();
				}
			}
		}
		
		//IMPORTANT: see note above on this variable as to why we're returning this instead of chocolateBoilerInstance.
		return tmpInstance;  
	}

	public void fill(){
		if(isEmpty()){
			empty = false;
			boiled = false;
			//fill boiler
		}
	}
	
	public void drain(){
		if(!isEmpty() && isBoiled()){
			//drain the boiled milk and chocolate
			empty = true;
		}
	}
	
	public void boil(){
		if(!isEmpty() && !isBoiled()){
			//bring the contents to a boil
			boiled = true;
		}
	}
	
	public boolean isEmpty(){
		return empty;
	}
	
	public boolean isBoiled(){
		return boiled;
	}
	
}
