package Intro;
/*
 * Subclass of Turkey. It just has simple implementations.
 */
public class WildTurkey implements Turkey {

	@Override
	public void goble() {
		System.out.println("Gobble Gobble");
	}

	@Override
	public void fly() {
		System.out.println("I'm flying a short distance");
	}

}
