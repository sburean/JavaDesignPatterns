package Intro;
/*
 *  Duck subclasses cannot directly use Turkey sub-classes in their place because of the different interfaces.
 *  This class will adapt the turkey interface (adaptee) to the duck interface (target)
 *  
 *  Adapters:
 *  - Implement the Target Interface. (What the client expects to see -> something that only uses duck objects)
 *  - Associated with the Adaptee Interface. (Gets a reference to the object we're adapting in the constructor)
 *  - Implements all of the Target Interface's methods. 
 *  - Figure out how to map methods from the Target Interface to the Adaptee.
 *  
 *  NOTE:
 *  - Job of implementing an adapter is proportional to the size of the interface we need to support (Target Interface)
 *  - Adapters can wrap more than one adaptee. Can have as many as needed to implement the target interface. 
 *  - Adapters can also implement multiple target interfaces to provide flexibility. (Can work with a system that has multiple exposed interfaces) 
 *  
 *  IMPORTANT:
 *  - The Adapter pattern acts to decouple the client from the incompatible interface. 
 *  - Can use this adapter (object adapter - based on composition) with adaptee subclasses!
 */

public class TurkeyAdapter implements Duck{
	
	Turkey turkey;
	
	public TurkeyAdapter(Turkey turkey) {
		// Get a reference to the object we're adapting
		this.turkey = turkey; 
	}
	
	/* Implements all of the Target Interface's methods. Each method maps functionality to the adaptee */
	
	@Override
	public void quack() {
		turkey.goble();
	}

	@Override
	public void fly() {
		/* Turkeys only fly short distances in small spurts, so to make it look like a duck's continuous fly, call it 5 times. */
		for (int i = 0; i < 5; i++) {
			turkey.fly();
		}
	}
}
