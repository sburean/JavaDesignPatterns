package Intro;
/*
 * This is the client. It would like to use a Turkey object in place of a duck object in our "testDuck" method, but it can't directly because of different interfaces. 
 * 
 * - So we use an adapter to adapt a turkey into a duck. 
 * - The client (testDuck method) would then make a request to the adapter by calling a method on it, using the Target-Interface.
 * - Adapter will translate this request into one or more calls on the adaptee, using the Adaptee-Interface.
 * - The client will receive the results of the call, without knowing an adapter is doing the translation (adapter remains transparent)
 */

public class TestStub {

	public static void main(String[] args) {

		/* We want test all of our objects with the testDuck method, but obviously we can't just pass it a turkey object.. */
		
		Duck duck = new MallardDuck();//a duck
		Turkey turkey = new WildTurkey();//a turkey
		
		/* So create an adapter and wrap the turkey in our adapter to make it look like a duck */
		Duck adapter = new TurkeyAdapter(turkey);

		//Now test the actual duck:
		System.out.println("The duck says:\n------------------");
		testDuck(duck);
		
		System.out.println("==================");
		
		//And now test the turkey that looks like a duck:
		System.out.println("The turkey that looks like a duck says:\n------------------");
		testDuck(adapter);
	}
	
	static void testDuck(Duck duck){
		/* Small test method for duck to call its methods */
		duck.quack();
		duck.fly();
	}

}
