package Intro;
/*
 * The adaptee interface. It is the interface that we're wrapping/adapting, or adapting/converting from.
 */
public interface Turkey {
	public void goble();
	public void fly();
}
