package Intro;
/*
 * The target interface. It is what we're adapting/converting to.
 */

public interface Duck {
	public void quack();
	public void fly();
}
