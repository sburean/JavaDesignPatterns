package Example;
import java.util.Enumeration;
import java.util.Iterator;

/*
 * If we have legacy client code that depends on the Enumeration interface, we want to have an adapter
 * that can allow us to use code that only has the Iteration interface. (ie: adapt an iterator to be usable as an enumeration)
 * 
 * -> allow client code that relies on enumeration to work with code that only uses iterations. 
 */

public class IteratorToEnumerationAdapter implements Enumeration<Integer> {
	
	Iterator<Integer> iterator;
	
	public IteratorToEnumerationAdapter(Iterator<Integer> iterator) {
		this.iterator = iterator;
	}

	/* Delegate enumeration method to the iterator */
	@Override
	public boolean hasMoreElements() {
		return iterator.hasNext();
	}

	@Override
	public Integer nextElement() {
		return iterator.next();
	}

}
