package Example;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

public class TestStub {

	public static void main(String[] args) {

		/* 
		 * Our legacy code (useEnumaration method) only uses the Enumeration interface 
		 * BUT ArrayLists don't support enumerations. Can't do "Enumeration<..> enumeration = testArray.enumeration();"
		 */
		
		ArrayList<Integer> testArray = new ArrayList<>(); //create a small object collection:
		testArray.add(1);
		testArray.add(2);
		testArray.add(3);
		testArray.add(4);
		
		Iterator<Integer> iterator = testArray.iterator(); //ArrayList supports iterators, so create one.
//		iterator.next(); //iterator operations possible.. but the legacy code ONLY USES enumerations, so an iterator is useless by itself
		
		//So instantiate our adapter that adapts an iteratorInterface to an enumerationInterface
		Enumeration<Integer> iterator2enumerationAdapter = new IteratorToEnumerationAdapter(iterator); //wrap the iterator
		
		/* So code that can relies on enumerations can use the following line to use iterations as enumerations on the ArrayList */
		useEnumeration(iterator2enumerationAdapter);

	}
	
	static public void useEnumeration(Enumeration<Integer> enumeration){
		/* Legacy code that uses the Enumeration interface, but our ArrayList (in main() above) only supports iterators. */
		
		while(enumeration.hasMoreElements()){
			//While we have elements in our collection, print them out.
			System.out.println(enumeration.nextElement());
		}
	}
}
