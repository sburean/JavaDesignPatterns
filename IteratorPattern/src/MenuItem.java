/*
 * A class whose objects will be used in various collections in this example. 
 */

public class MenuItem {

	String name;
	String description; 
	boolean vegetarian; 
	double price;
	
	public MenuItem(String name, String description, boolean vegetarian, double price)
	{
		this.name = name; this.description = description; this.vegetarian = vegetarian; this.price = price;
	}
	
	/* Getter methods to access data fields of the menuItem class */
	public String getName() { 
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public double getPrice() {
		return price;
	}
	
	public boolean isVegetarian() { 
		return vegetarian;
	}
	
}
