import java.util.Iterator;

/*
 * The client (Waitress). It contains methods to:
 * - Print complete menus for customers (either breakfast, dinner, both, or vegetarian items only)
 * - Specify if a menu item is vegetarian or not
 * 
 * IMPORTANT: 
 * The client (this waitress) will now get iterator objects from the appropriate collection it needs to traverse.
 * It doesn't know specifics about the implementation of the iterators OR the collections themselves! (they are encapsulated in the iterator itself) 
 * Client is also decoupled from concrete menu objects because they share the same interface, and this is written against the menu iterface.
 * ( Program to an interface, NOT an implementation ) -> Reduces dependency between client and concrete objects (concrete menus)
 */

public class Waitress {
	
	private Menu pancakeHouseMenu, dinerMenu, cafeMenu;

	/**
	 * Constructor two menu objects.
	 */
	public Waitress(Menu pancakeHouseMenu, Menu dinerMenu, Menu cafeMenu) {
		this.pancakeHouseMenu = pancakeHouseMenu;
		this.dinerMenu = dinerMenu;
		this.cafeMenu = cafeMenu;
	}
	
	/**
	 * Creates two iterators, one for each menu.
	 * It then calls the overloaded printMenu() method with each iterator.
	 */
	public void printMenu() {
		/* NOTE: This method violates the open closed principle. 
		 	New menu iterators will have to be created here, and menu display needs to be updated.
		 	SEE COMPOSITE PATTERN FOR SOLUTION */
		Iterator<MenuItem> pancakeIterator = pancakeHouseMenu.getIterator();
		Iterator<MenuItem> dinerIterator = dinerMenu.getIterator();
		Iterator<MenuItem> cafeIterator = cafeMenu.getIterator(); //new iterators are easily initialized
		
		System.out.println("MENU\n----\nBREAKFAST:");
		printMenu(pancakeIterator);
		
		System.out.println("\nLUNCH:");
		printMenu(dinerIterator);
		
		System.out.println("\nDINNER:"); 
		printMenu(cafeIterator);
	}
	
	/**
	 * Overloaded printMenu(Iterator ..) uses the iterator to step through a collection (of one type) and print them.
	 * 
	 * Contains a loop that polymorphically handles any collection of objects (of one type) as long as it utilizes an iterator.
	 * IMPORTANT: The iterator may step through VARIOUS collections, but they must be of the same type! 
	 * 				(ie: menuItems stored as ArrayList, an Array[], etc... )
	 */
	private void printMenu(Iterator<MenuItem> iterator) {
		/* This code doesn't change if new iterators are added; all iterators over MenuItem objects may be used here. */
		
		while (iterator.hasNext()) { //while we have objects to iterate through..
			MenuItem menuItem = iterator.next();
			System.out.print(menuItem.getName() + ", ");
			System.out.print(menuItem.getPrice() + " -- ");
			System.out.println(menuItem.getDescription());
		}
	}
	
	//other methods..

}
