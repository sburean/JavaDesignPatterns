import java.util.ArrayList;

/*
 * The concrete iterator. Associated with a collection of objects, and provides specific implementation for methods
 * - to iterate through the collection. 
 * 
 * Objects of this class will be hooked into appropriate classes containing the collection, and used to - 
 * - iterate through the collection. 
 * 
 * Encapsulates ALL iteration logic, and decouples the client from the collection!
 * 
 * IMPORTANT NOTE: This custom iterator isn't fail-safe. If underlying collection changed after iterator created, 
 * 					behavior will be undetermined. 
 * 
 */

/**
 * This class was used as an example to show how an iterator would be implemented from scratch. 
 * PancakeHouseMenu's ArrayList will now use Java.util.Iterator instead since ArrayList natively supports the iterator
 */
public class PancakeHouseIterator implements MyIterator {
	
	ArrayList<MenuItem> items;
	int position = 0; // maintains current position of iterator over array
	
	public PancakeHouseIterator(ArrayList<MenuItem> menuItems) {
		this.items = menuItems;
	}
	
	@Override
	public boolean hasNext() {
		/* Returns true if an element exists at position + 1, false otherwise 
		 	NOTE: Assumes array is fully filled! */
		
		if( position < items.size() && !items.contains(null) ){
			//While we're within ArrayList bounds AND the item isn't null, we have a next item.
			return true;
		} else {
			return false; //ex: 3 items in ArrayList, when we have no next item, position will be 2.
		}
	}

	@Override
	public Object next() {
		/* Returns next item of the array and increments position */
		MenuItem menuItem = items.get(position);
		position++;
		return menuItem;
	}

}
