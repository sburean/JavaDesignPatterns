import java.util.Iterator; //using the native java iterator interface.

/*
 * A class containing an Array collection. It has many methods that rely on the Array.
 * "The diner menu" - An Aggregate / Container.
 * 
 * IMPORTANT: Each menu class assumes the responsibility of creating a concrete iterator -
 * 				- that is appropriate for its internal collection implementation.
 * 			However, the actual iterator implementation is the responsibility of a separate class!
 */

public class DinerMenu implements Menu {

	static final int MAX_ITEMS = 6; 
	int numberOfItems = 0;
	MenuItem[] menuItems;
	
	public DinerMenu() { 
		
		menuItems = new MenuItem[MAX_ITEMS];
		
		addItem("Vegetarian BLT", "(Fakin') Bacon with lettuce & tomato on whole wheat", true, 2.99);
		addItem("BLT", "Bacon with lettuce & tomato on whole wheat", false, 2.99);
		addItem("Soup of the day", "Soup of the day, with a side of potato salad", false, 3.29);
		addItem("Hotdog", "A hot dog, with saurkraut, relish, onions, topped with cheese", false, 3.05);
		addItem("Steamed Veggies and Brown Rice", "Steamed veggies over brown rice", true, 3.99);
		addItem("Pasta", "Spaghetti with marinara Sauce, and a slice of sourdough bread", true, 3.89);
		
	}
	
	public void addItem(String name, String description, boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price); 
		
		if (numberOfItems >= MAX_ITEMS) {
			/* Check to see if menu is full, give an error if it is */
			System.err.println("Sorry, menu is full! Can’t add item to menu"); 
		} else {
			menuItems[numberOfItems] = menuItem;
			numberOfItems = numberOfItems + 1; 
		}
		
	}
	
//	public MenuItem[] getMenuItems(){
//		return menuItems;
//	}
	
	/**
	 * An example of a method needed if writing own custom iterator.
	 * See "DinerMenuItarator" for concrete implementation. (Encapsulated in separate class)
	 */
	public Iterator<MenuItem> getIterator(){
		/* 
		 * Getter method replaced by a method that returns an ITERATOR over the contained collection of menuItems 
		 * to the client. This encapsulates collection AND iterator implementations.
		 * 
	 	 * NOTE: The getter "getMenuItems()" exposes the contained collection. Client sees it's an Array. 
	 	 * */
		return new DinerMenuIterator(menuItems);
	}

	//Other methods here...
}
