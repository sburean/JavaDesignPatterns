/*
 * TestStub simply creates a waitress object (the client) and calls methods such as printMenu(), etc..
 */

public class TestStub {
	
	public static void main(String[] args){
	
		//First, create menu objects that waitress needs in constructor:
		Menu dinerMenu = new DinerMenu();
		Menu pancakeHouseMenu = new PancakeHouseMenu();
		Menu cafeManu = new CafeMenu();
		
		//Create waitress:
		Waitress waitress = new Waitress(pancakeHouseMenu, dinerMenu, cafeManu);
		
		//Now call waitress methods to show display menus:
		waitress.printMenu();
		
	}
	
	
}
