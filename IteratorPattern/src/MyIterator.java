/*
 * The iterator interface. Contains methods that helps iterate through a collection of objects.
 * Concrete iterators will implement this and provide specific implementations.
 * 
 * Helps reduce the issues brought along by having different types of collections used in different classes. 
 * - Clients wishing to use multiple types of collections will have varying code used for iterating through each collection. 
 * - IMPORTANT: Concrete classes with this interface will encapsulate the iteration (area of change)
 * 
 * NOTE: Can extend java.util.Iterator to provide extra functionality.
 */

public interface MyIterator {
	
	/* Two methods for this iterator */
	
	/**
	 * Returns true if there are more elements in the collection from the current index; false otherwise.
	 */
	boolean hasNext(); 
	
	/**
	 * Returns the next object in the collection.
	 */
	Object next();
}
