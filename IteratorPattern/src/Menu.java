import java.util.Iterator;

/*
 * A common interface to-be-implemented by aggregates; classes containing collections.
 * --> Decouples client from implementation of collection <--
 * 
 * In this example, the two menus contain a collection of menuItems.
 * 
 * AKA: "Aggregate", "Container"
 */

public interface Menu {

	public Iterator<MenuItem> getIterator(); 
}
