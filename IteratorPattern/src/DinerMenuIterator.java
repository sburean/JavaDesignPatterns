import java.util.Iterator;

/* 
* The concrete iterator. Associated with a collection of objects, and provides specific implementation for methods
 * - to iterate through the collection. 
 * 
 * Objects of this class will be hooked into appropriate classes containing the collection, and used to - 
 * - iterate through the collection. 
 * 
 * Encapsulates ALL iteration logic, and decouples the client from the collection!
 * 
 * IMPORTANT NOTE: This custom iterator isn't fail-safe. If underlying collection changed after iterator created, 
 * 					behavior will be undetermined. 
 */

public class DinerMenuIterator implements Iterator<MenuItem> {
	
	MenuItem[] items;
	int position = 0; // maintains current position of iterator over array
	
	public DinerMenuIterator(MenuItem[] menuItems) {
		this.items = menuItems;
	}
	
	@Override
	public boolean hasNext() {
		/* Returns true if an element exists at position + 1, false otherwise 
		 	NOTE: Assumes array is fully filled! */
		
		if( position < items.length && items[position] != null ){ 
			// if we're within array bounds, AND the item at the current position isn't null, we have an item available. 
			return true;
		} else {
			return false;
		}
	}

	@Override
	public MenuItem next() {
		/* Returns next item of the array and increments position. */
		MenuItem menuItem = items[position];
		position++;
		return menuItem;
	}
	
	@Override
	public void remove() throws IllegalStateException {
		/* [OPTIONAL] Removes the last item returned by next() from the underlying collection. 
		 	-> (simply shifts all other items down one index position) */
		
		if(position <= 0){
			/* when remove is called before next; throw exception */
			throw new IllegalStateException("Error: Cannot remove before calling next");	
		} 
		
		if( items[position-1] != null ){
			/* if we have an item to remove */
			
			for (int i = (position - 1); i < (items.length - 1); i++) { 
				/* from the index of the item we're removing (position - 1) to the SECOND LAST INDEX 
				 	-> (b/c we're copying from i+1; so otherwise at end of array we would access an out of bounds location) */
				
				//shift all items down
				items[i] = items[i+1];
				
			}
			
			/* nullify the last entry of the array; we've removed an entry and shifted everything down. */
			items[items.length-1] = null; 
			
		}
		
		
	}

}
