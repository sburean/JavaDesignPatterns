import java.util.ArrayList;
import java.util.Iterator; //we use the native java iterator

/*
 * A class containing an ArrayList collection. It has many methods that rely on the ArrayList.
 * "The breakfast menu"  - An Aggregate / Container.
 * 
 * IMPORTANT: Each menu class assumes the responsibility of CREATING a concrete iterator -
 * 				- that is appropriate for its internal collection implementation.
 * 			However, the actual iterator implementation is the responsibility of a separate class!
 */

public class PancakeHouseMenu implements Menu {

	ArrayList<MenuItem> menuItems;
	
	public PancakeHouseMenu() { 
		
		menuItems = new ArrayList<>();
	
		addItem("K&B's Pancake Breakfast", "Pancakes with scrambled eggs, and toast", true, 2.99);
		addItem("Regular Pancake Breakfast", "Pancakes with fried eggs, sausage", false, 2.99);
		addItem("Blueberry Pancakes", "Pancakes made with fresh blueberries", true, 3.49);
		addItem("Waffles", "Waffles, with your choice of blueberries or strawberries", true, 3.59);
		
	}
	
	public void addItem(String name, String description, boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price); 
		menuItems.add(menuItem);
	}
	
//	public ArrayList<MenuItem> getMenuItems(){
//		return menuItems;
//	}
	
	/**
	 * An example of a method needed if writing own custom iterator.
	 * See "PancakeHouseItarator" for concrete implementation. (Encapsulated in separate class)
	 */
//	public MyIterator getIterator(){
//		/* 
//		 * Getter method replaced by a method that returns an ITERATOR over the contained collection of menuItems 
//		 * to the client. This encapsulates collection AND iterator implementations. 
//		 * 
//	 	 * NOTE: The getter "getMenuItems()" exposes the contained collection. Client sees it's an ArrayList<E>. 
//	 	 * */
//		return new PancakeHouseIterator(menuItems);
//	}
	
	public Iterator<MenuItem> getIterator(){
		/* With the native iterator, simply return the ArrayList's iterator */
		return menuItems.iterator();
	}
	
	//Other methods here...
}
