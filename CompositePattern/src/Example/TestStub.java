package Example;
/*
 * Test Stub. Creates all components and appends children to appropriate parent nodes. 
 * 
 * NOTE: Need to learn how to write a tree class.
 */

public class TestStub {
	
	public static void main(String[] args){
		
		Component start = new Composite(1);

		Component root = new Composite(1);
		
		//Level1:
		Component child11 = new Composite(2);
		Component child12 = new Composite(3);
		Component child13 = new Composite(4);
		
		//Level2:
		Component child21 = new Leaf(5);
		Component child22 = new Leaf(6);
		Component child23 = new Composite(7);
		Component child24 = new Leaf(8);
		Component child25 = new Composite(9);
		Component child26 = new Composite(10);
		
		//Level3:
		Component child31 = new Leaf(11);
		Component child32 = new Leaf(12);
		Component child33 = new Leaf(13);
		Component child34 = new Leaf(14);
		
		//Connect starting point to root: (only needed if we want to display the root data; usually we don't if we're iterating)
		start.addChild(root);
		
		//Connect Level1 to root:
		root.addChild(child11);
		root.addChild(child12);
		root.addChild(child13);
		
		//Connect Level2 children to parents in Level1:
		child11.addChild(child21);
		
		child12.addChild(child22);
		child12.addChild(child23);
		child12.addChild(child24);
		
		child13.addChild(child25);
		child13.addChild(child26);
		
		//Connect Level3 children to parents in Level2:
		child23.addChild(child31);
		
		child25.addChild(child32);
		child25.addChild(child33);
		
		child26.addChild(child34);
		
		//Now create client:
		Client client = new Client(start);
		client.printTreeData();
		
	}

}
