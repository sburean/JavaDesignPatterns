package Example;
import java.util.Iterator;

/*
 * Client accepts the root of a structure in the constructor; (root of structure to-be-traversed)
 * The client gets a CompositeIterator from root and traverses its subtrees with the composite iterator's next() and hasNext(). 
 * It may perform operations (call Component methods) on components it traverses over.
 */

public class Client {

	private Component root;
	/* NOTE: could just as easily taken the compositeIterator over the root directly; 
	 * but this encapsulates getting the iterator into this client */
	
	public Client(Component root) {
		this.root = root;
	}
	
	public void printTreeData(){
		/* uses hasNext() and next() of the compositeIterator to traverse a structure */
		Iterator<Component> compositeIterator = root.getCompositeIterator();
		while(compositeIterator.hasNext()){
			compositeIterator.next().printData();
		}
	}
	
}
