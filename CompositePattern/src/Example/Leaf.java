package Example;
import java.util.Iterator;

/*
 * Leaf objects; they hold data and contain no children.
 */

public class Leaf extends Component {
	
	private int data;
	
	public Leaf(int data) {
		this.data = data;
	}
	
	@Override 
	public void printData(){
		System.out.println("Node Data: " + data);
	}
	
	@Override
	public Iterator<Component> getCompositeIterator(){
		return new NullIterator();
	}

}
