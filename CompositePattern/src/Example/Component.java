package Example;
import java.util.Iterator;

/*
 * The common superType for all nodes within the composite structure (Tree).
 * 
 * Holds methods that all components may have. Note that components override appropriate methods only.
 * Default behavior of each method is to throw UnsupportedOperationException.
 */

public abstract class Component {

	public void printData(){
		throw new UnsupportedOperationException();
	}
	
	public void addChild(Component n){
		throw new UnsupportedOperationException();
	}
	
	public Iterator<Component> getCompositeIterator(){
		throw new UnsupportedOperationException();
	}
	
	
}
