package Example;
import java.util.Iterator;

/*
 * A composite object. It contains data and other components as children; stored in a collection.
 */

public class Composite extends Component {
	
	private final static int MAX_CHILDREN = 4; // n = 4 for our "n-ary" tree
	private Component[] children;
	private int index = 0;
	private int data;
	private Iterator<Component> iterator;
	
	
	public Composite(int data) {
		this.children = new Component[MAX_CHILDREN];
		this.data = data;
	}
	
	@Override
	public void printData(){
		System.out.println("Node Data: " + data);
	}
	
	@Override
	public void addChild(Component child){
		
		/* Add a child to our array if there's room */
		if(index <= children.length){
			children[index] = child;
			index++;
		} else {
			System.err.println("ERROR: Can't add child. Maximum number of children reached.");
		}
	}
	
	@Override
	public Iterator<Component> getCompositeIterator(){
		/* Returns a composite-iterator over this composite's children */
		
		if(iterator == null){
			iterator = new CompositeIterator(new CollectionsIterator(children));
		}
		return iterator;
		
		//TODO: try creating a new one each time, see what happens. (without returning this.iterator)
	}
	

}
