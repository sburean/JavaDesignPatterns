package Example;
import java.util.Iterator;

/*
 * Null iterator to prevent constant null checks. 
 * Always returns false on hasNext(); and null for next(). [which shouldn't be called]
 */

public class NullIterator implements Iterator<Component>{

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public Component next() {
		return null;
	}
	
}
