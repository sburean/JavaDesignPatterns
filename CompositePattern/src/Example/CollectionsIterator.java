package Example;
import java.util.Iterator;

/*
 * An iterator object over a java array since it's not natively supported.
 * Array that needs to be traversed is passed into the constructor. 
 */

public class CollectionsIterator implements Iterator<Component>{

	private Component[] collection;
	private int index = 0;
	
	public CollectionsIterator(Component[] collection) {
		this.collection = collection;
	}
	
	@Override
	public boolean hasNext() {
		/* Returns true while there are more elements to process in the array */
		
		if(index < collection.length && collection[index] != null){
			//if we're within bounds && item at index isn't null
			return true;
		} else {
			return false;
		}

	}

	@Override
	public Component next() {
		/* Returns the <T> object at the index of the iterator, and increments index AFTER. */
		
		//IMPORTANT: Note the lack of null check; assumes next() here is only called after a hasNext() check.
		Component objToReturn = collection[index];
		index++;
		return objToReturn;
	}
	
	@Override
	public void remove(){
		/* Use to remove last element returned by next() */
		
		//Removes element at index-1; and shifts all others down by one in the array. See "IteratorPattern/DinerMenuIterator"
		throw new UnsupportedOperationException("ERROR: remove() is not supported.");
	}

}
