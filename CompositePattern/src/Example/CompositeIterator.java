package Example;
import java.util.Iterator;
import java.util.Stack;

/*
 * CompositeIterator is used whenever we need to iterate over a compositeStructure (ie: a hierarchical structure) 
 * (where components might contain children themselves) 
 * 
 * A collections iterator is used to iterate over a linear structure (one level; no hierarchy) 
 */

public class CompositeIterator implements Iterator<Component>{

	Stack<Iterator<Component>> stack = new Stack<>();
	
	public CompositeIterator(Iterator<Component> collectionIterator) {
		/* 
		 * Push a collection-iterator over the root node to iterate over all components at this level.
		 */
		stack.push(collectionIterator);
	}
	
	@Override
	public boolean hasNext() {
		/*
		 * Returns true if there are any more components left to visit (& process) in the structure:
		 * - Check at the level of the iterator on top of the stack.
		 * - If iterator on top of stack is fully traversed, remove it from the stack.
		 * 		-> recursively call hasNext() to see if next iterator (if any) on the stack has components left to visit
		 * Returns false is nothing left on stack to process.
		 */
		
		if(!stack.isEmpty()){
			
			if(stack.peek().hasNext()){
				//If iterator on top of the stack has more component to process
				return true;
			} else {
				stack.pop(); // remove iterator from stack if it's been fully processed (all components visited)
				return hasNext(); // recursively call hasNext() and see if next iterator on stack (if any) has anything to process.
			}
			
		} else {
			return false;	
		}
		
	}

	@Override
	public Component next() {
		/*
		 *  Returns the <T> object at the current index of the iterator on top of the stack.
		 *  But first have to check if the <T> object is an instance of a composite; it will then contain children.
		 *  	-> Have to create a new CompositeIterator over the children to process them; push it onto stack. 
		 */
		
		if(hasNext()){
			
			Component objToReturn = stack.peek().next(); //get object from stack's top iterator, and increment iterator's index.
			if(objToReturn instanceof Composite){
				//object has children, create new CompositeIterator and push it onto stack to be processed next
				//(ie: to traverse along this composite's children before continuing at the level we're currently on)
				stack.push(objToReturn.getCompositeIterator());
			}
			return objToReturn;
			
		} else {
			/*
			 * Nothing left to process, return null. 
			 * This shouldn't happen. Smart client's only call next() on
			 * a CompositeIterator after checking its hasNext() return value. 
			 */
			return null;
		}
	}

}
