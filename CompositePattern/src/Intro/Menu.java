package Intro;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * A common superType to-be-used by aggregates. (Classes containing collections)
 * --> Decouples client from implementation of collection <--
 * 
 * In this example, the menus contain a collection of menuItems.
 * AKA: "Aggregate", "Container"
 * 
 * Considered as a "Composite" element as it may contain other children of the type "MenuComponent". 
 * It will override appropriate methods from the MenuComponent superType.
 */

public class Menu extends MenuComponent {
	
	/* IMPORTANT: Our ENTIRE TREE STRUCTURE will use ArrayList collections! */
	
	private ArrayList<MenuComponent> menuComponents = new ArrayList<>(); //A composite contains other MenuComponents (children)
	private String name, description; //each menu will have a name and an implementation
	private Iterator<MenuComponent> iterator = null; // an iterator over MenuComponents (any object in the Composite Structure)
	
	public Menu(String name, String desc) {
		this.name = name;
		this.description = desc;
	}
	
	/* Composite elements override MenuComponent methods that deal with managing other components */
	@Override
	public void add(MenuComponent menuComponent) { 
		menuComponents.add(menuComponent);
	}
	
	@Override
	public void remove(MenuComponent menuComponent) { 
		menuComponents.remove(menuComponent);
	}
	
	@Override
	public MenuComponent getChild(int index) {
		return menuComponents.get(index);
	}
	
	/* Getter methods for retrieving Menu name & description */
	public String getName() { 
		return name;
	}
	
	public String getDescription() { 
		return description;
	}
	
	@Override
	public void print() {
		/* The print method; each menu will print its name and description.
		 * NOTE: We rely on each component of this composite to print itself!
		 */
		System.out.print("\n" + getName());
		System.out.println(", " + getDescription()); 
		System.out.println("---------------------");
		
		/* 
		 	Now as each MenuComponent of this composite implements the print() method, loop through each component and
		 	call print() on it!
		 	(recursively printing each component within this composite) 
		 */
		
		/*
		 * IMPORTANT: Notice if while we're iterating the ArrayList and we encounter ANOTHER composite (a Menu),
		 *				the print() method is recursively called!!  
		 */
					
		Iterator<MenuComponent> iterator = menuComponents.iterator(); //iterator of MenuComponents (Leaves and Composites)
		while (iterator.hasNext()) {
			MenuComponent menuComponent = iterator.next();
			menuComponent.print(); //Each menuComponent prints itself. 
			
			/* NOTE: The native collections iterator is fine if we're recursively calling a MenuComponent method. */
			
		}
	}
	
	@Override
	public void printVegeterian(){
		Iterator<MenuComponent> tstIterator = menuComponents.iterator();
		//using internal (collections) RECURSIVELY iterators to traverse whole structure and print vegetarian only items:
		//(notice how this isn't in the client, so client has no control over iteration if we use internal iterations to recursively traverse)
		
		while(tstIterator.hasNext()){ //while we have nodes to traverse
			
			MenuComponent node = tstIterator.next(); //get the current node
			
			if(node instanceof Menu){ //if it's a menu (composite), 
				
				node.printVegeterian(); //recursively call this method on it so it traverses all of its children (use implicit stack; program stack)
				
			} else { //a leaf node, print it if it's a vegetarian menu item
				
				if (node.isVegetarian()) {
					node.print();
				}
			}
		}
	}
	
	@Override
	public Iterator<MenuComponent> createIterator() {
		
		//Create an external (CompositeIterator) over this composite's components.
		
		if(iterator == null){
			iterator = new CompositeIterator(menuComponents.iterator());
		}
		
		return iterator;
	}
	
}
