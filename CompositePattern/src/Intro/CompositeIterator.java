package Intro;
import java.util.Iterator;
import java.util.Stack;

/**
 * The CompositeIterator (an external iterator) is tasked with iterating over ALL MenuComponents in the composite structure.
 * If a component is a composite, it then needs to iterate over that composite's components as well. (and so on...)
 * Advantage of external iterator over using internal iterator to traverse is it gives more flexibility to code, 
 * 		(allowing client to control it), but harder to implement as need to keep track of traversal hierarchy. (use explicit stack)  
 *
 * This is a tree-traversal (search) algorithm -> NEED TO STUDY THIS SEPARATELY. 
 */
public class CompositeIterator implements Iterator<MenuComponent> {
	
	Stack<Iterator<MenuComponent>> stack = new Stack<>(); // Stack holds iterators that still have elements to be processed.

	public CompositeIterator(Iterator<MenuComponent> topLvLiterator) {
		stack.push(topLvLiterator); // Automatically add iterator over root's components (have to process each of its elements to completely process composite structure)
	}
	
	/**
	 * Returns TRUE if stack contains iterators with more items (components) to be processed, false otherwise.
	 */
	@Override
	public boolean hasNext() {
		
		if(stack.isEmpty()){
			/* If stack is empty, nothing left to process. */
			return false;
		} else {
			/* We have iterators with more elements to process. */
			
			Iterator<MenuComponent> iteratorToProcess = stack.peek(); // get top iterator from the stack that we have to process (without removing it)
			
			if(iteratorToProcess.hasNext()){
				// Top Iterator has an element to process: (NOTE: if hasNext() == true, next() should ALWAYS return an element)
				return true;
			} else {
				/* 
				 * Top Iterator has no more elements to process, pop it off the stack. (iterator traversed all of its elements)
				 * Then RECURSIVELY call hasNext() (& return result) to see if next iterator on stack has elements to be processed, or if we're done.
				 */
				stack.pop();
				return hasNext();
			}
			
		}
		
	}

	/**
	 * Returns the component at the index of the iterator currently at the top of the stack.
	 * If the component is a composite, it will also create a new CompositeIterator with the 
	 * 	composite component as the root, and pushes the newly created iterator on-top of the stack.
	 */
	@Override
	public MenuComponent next() {
		
		if(hasNext()){ // If we have an element to return / process:
			
			MenuComponent component = stack.peek().next(); //get component and increment iterator's index.
			if (component instanceof Menu){
				/* component is a composite, create a new CompositeIterator over its children & push it onto stack for processing */
				stack.push(component.createIterator());
			}
			return component; //return the component
			
		} else {
			/*
			 * Nothing left to process, return null. 
			 * This shouldn't happen. Smart clients will only call next() on
			 * a CompositeIterator after checking its hasNext() return value. 
			 */
			return null;
		}
		
	}
	
}
