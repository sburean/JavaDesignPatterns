package Intro;
import java.util.Iterator;

/*
 * A class whose objects will be used in various collections in this example.
 * 
 * Considered as a "Leaf" element as it contains no children. 
 * It will override appropriate methods from the MenuComponent superType.
 */

public class MenuItem extends MenuComponent {

	String name;
	String description; 
	boolean vegetarian; 
	double price;
	
	public MenuItem(String name, String description, boolean vegetarian, double price)
	{
		this.name = name; this.description = description; this.vegetarian = vegetarian; this.price = price;
	}
	
	/* Leaf elements override MenuComponent getter methods to access data fields of the menuItem class */
	@Override
	public String getName() { 
		return name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public double getPrice() {
		return price;
	}
	
	@Override
	public boolean isVegetarian() { 
		return vegetarian;
	}
	
	@Override
	public void print(){
		/* Now overriding the print method from the MenuComponent SuperType; will now print complete menuItem */
		System.out.print(" " + getName()); 
		if (isVegetarian()) {
			System.out.print("(v)"); 
		}
		System.out.println(", " + getPrice());
		System.out.println(" -- " + getDescription());
	}
	
	@Override
	public Iterator<MenuComponent> createIterator(){
		/* The null iterator is another example of the NullObject "Design Pattern" */
		return new NullIterator();
	}
	
}
