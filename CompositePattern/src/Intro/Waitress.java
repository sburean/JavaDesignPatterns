package Intro;
import java.util.Iterator;

/*
 * The client (Waitress). It contains a method to:
 * - Print complete menus for customers (either breakfast, dinner, both, or vegetarian items only)
 * - Print only vegeterian items
 * 
 * IMPORTANT:
 * The client gets a hold of the root composite element, and simply performs operations on the root.
 * Because of the tree-structure of the composite pattern, all leaves and composites are treated uniformly, and
 * 	- any method call on a composite will recursively be called on all of its children. 
 *  
 */

public class Waitress {
	
	MenuComponent allMenus;
	
	public Waitress(MenuComponent root) {
		
		/* 
		 * We simply hand the client the top-level component.
		 * 	-> Since each object in the tree is of type MenuComponent, we can recursively call a method on each one.
		 */
		
		this.allMenus = root;
	}
	
	public void printMenu(){
		/* call the print() method on the entire hierarchy (recursively) by calling it on the root */
		
		allMenus.print();
	}
	
	public void printVegeterianInternal(){
		
		/* 
		 * Uses the internal iterator (collections-Iterator) to RECURSIVELY traverse over whole tree and visit each node.
		 * If a node is a MenuItem (Leaf), then if it's a vegetarian item we print it.
		 * (notice how traversal is implemented in the Composite itself; client has no control)
		 */
		
		System.out.println("\nVEGETARIAN MENU\n----");
		allMenus.printVegeterian();
	}
	
	public void printVegeterianExternal(){
		/* 
		 * Use the externalIterator (composite-Iterator) to ITERATIVELY traverse over the whole tree and visit each node.
		 * We then print all of the MenuItems that are vegetarian. 
		 * (Notice how client has control with the externalIterator; explicitly calls hasNext() and next() to traverse)
		 */
		
		Iterator<MenuComponent> iterator = allMenus.createIterator();
		System.out.println("\nVEGETARIAN MENU\n----");
		while (iterator.hasNext()) {
			MenuComponent menuComponent = iterator.next();
			try {
				if (menuComponent.isVegetarian()) {
					menuComponent.print();
				}
			} catch (UnsupportedOperationException e) {} //Composites (Menus) don't implement this, and by default will throw an unsupported exception from the Component abstract calss.
			
			/* NOTE: Better solution (while keeping transparency) would be to return false in isVegetarian() in Menus */
		}
	}

}
