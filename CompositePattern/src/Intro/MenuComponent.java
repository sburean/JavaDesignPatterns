package Intro;
import java.util.Iterator;

/*
 * The component superType. It acts as the common interface for both the leaves and composite objects.
 * (This superType is what allows us to treat leaves and composites uniformly)
 * 
 * The component superType provides default implementations of methods such that if -
 * - a node or composite doesn't want to implement some of the superType methods, it can fall -
 * - back on some default behaviour. 
 */
public abstract class MenuComponent {
	
	/* Default behaviour for all methods is to throw an unsupportedOperationException.
	 	Concrete components will Override and provide specific implementations to appropriate (& supported) methods. */

	/* Composite methods: */
	public void add(MenuComponent menuComponent) { 
		throw new UnsupportedOperationException();
	}
	
	public void remove(MenuComponent menuComponent) {
		throw new UnsupportedOperationException(); 
	}
	
	public MenuComponent getChild(int i) {
		throw new UnsupportedOperationException();
	}
	
	public Iterator<MenuComponent> createIterator(){
		throw new UnsupportedOperationException();
	}
	
	/* Operation methods: */
	public String getName() {
		throw new UnsupportedOperationException();
	}
	
	public String getDescription() {
		throw new UnsupportedOperationException(); 
	}
	
	public double getPrice() {
		throw new UnsupportedOperationException();
	}
	
	public boolean isVegetarian() {
		throw new UnsupportedOperationException(); 
	}
	
	/**
	 * Both Leaves and Composites will implement this method.
	 * Allows each MenuComponent to print appropriate information. 
	 * (and each MenuComponent will be treated uniformly - simply calling their "print()" method!.
	 */
	public void print() {
		throw new UnsupportedOperationException();
	}
	
	public void printVegeterian(){
		throw new UnsupportedOperationException();
	}
	
}
