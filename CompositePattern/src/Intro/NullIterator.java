package Intro;
import java.util.Iterator;

/*
 *  It simply returns an iterator that always returns false on hasNext() to indicate there is nothing to iterate over 
 *  (as with all leaf nodes) 
 * 	-> Removes need to check for null whenever an iterator is returned in the structure. 
 */

public class NullIterator implements Iterator<MenuComponent> {

	public MenuComponent next() { 
		return null;
	}
	public boolean hasNext() { 
		return false;
	}
	
}
