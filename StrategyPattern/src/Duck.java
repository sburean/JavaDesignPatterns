/*
 * An abstract superclass "Duck". 
 * It delegates behavior through association to concrete behavior classes.
 * Subclasses will extend this to provide specific implementations where appropriate.
 */

public abstract class Duck {
	
	//The two super-type references that will delegate behaviour
	FlyBehaviour flyBehaviour;
	QuackBehaviour quackBehaviour;
	
	public Duck(){
		//empty constructor
	}
	
	public abstract void display();
	
	/* These two methods perform actions from the concrete behaviour classes */
	public void performFly(){
		flyBehaviour.fly();
	}
	
	public void performQuack(){
		quackBehaviour.quack();
	}
	
	/* NOTE: These next two setter methods allow setting behaviour dynamically. */
	public void setFlyBehaviour(FlyBehaviour fb){
		this.flyBehaviour = fb;
	}
	
	public void setQuackBehaviour(QuackBehaviour qb){
		this.quackBehaviour = qb;
	}
	
}
