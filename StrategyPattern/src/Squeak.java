/*
 * This is a concrete behaviour class.
 * Behaviour is encapsulated from the Duck class, and all changes to this behaviour is done here. 
 * Duck subclasses will call this behaviour as needed (through an association relationship)
 */

public class Squeak implements QuackBehaviour {

	@Override
	public void quack() {
		System.out.println("Squeak");
	}

}
