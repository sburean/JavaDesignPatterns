/*
 * A duck subclass. It inherits the behaviour super-types from the superclass.
 * Using association, we can first create and then dynamically change concrete behaviour classes. 
 */

public class ModelDuck extends Duck {

	public ModelDuck() {
		this.flyBehaviour = new FlyNowWay();
		this.quackBehaviour = new Quack();
	}
	
	@Override
	public void display() {
		System.out.println("I'm a model duck");
	}

}
