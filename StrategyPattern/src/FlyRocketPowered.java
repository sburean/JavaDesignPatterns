/*
 * This is a concrete behaviour class.
 * Behaviour is encapsulated from the Duck class, and all changes to this behaviour is done here. 
 * Duck subclasses will call this behaviour as needed (through an association relationship)
 */

public class FlyRocketPowered implements FlyBehaviour {

	@Override
	public void fly() {
		System.out.println("I'm flying with a rocket!");
	}

}
