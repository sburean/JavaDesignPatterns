/*	
 *	This is a behavior interface. 
 *	Concrete implementations of various flying behaviors will -
 *	- implement this interface, guaranteeing certain behavior (methods)
 */

public interface FlyBehaviour {
	
	public void fly();
	
}
