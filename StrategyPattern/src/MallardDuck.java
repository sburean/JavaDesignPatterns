/*
 * A duck subclass. It inherits the behaviour super-types from the superclass.
 * Using association, we can first create and then dynamically change concrete behaviour classes. 
 */

public class MallardDuck extends Duck {

	public MallardDuck(){
		this.flyBehaviour = new FlyWithWings();
		this.quackBehaviour = new Quack();
	}
	
	@Override
	public void display() {
		System.out.println("I'm a real Mallard duck!");
	}

}
