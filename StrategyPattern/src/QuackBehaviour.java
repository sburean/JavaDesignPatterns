/*	
 *	This is a behavior interface. 
 *	Concrete implementations of various quacking behaviors will 
 *	implement this interface, guaranteeing certain behavior (methods)
 */

public interface QuackBehaviour {

	public void quack();
	
}
