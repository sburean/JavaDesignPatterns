/*
 * This is a test stub for the Strategy Pattern.
 * We can create new instances of various behaviour classes -
 * - and dynamically assign then to duck subclasses at runtime (more flexibility, less maintenance).
 * Or we can create the various behaviour classes within the -
 * - duck subclass constructors (composition relationship), without setter methods for these relationships.
 */

public class MiniDuckSimulator {

	public static void main(String[] args) {
		Duck mallard = new MallardDuck();
		mallard.performFly();
		mallard.performQuack();
		
		Duck model = new ModelDuck();
		model.performFly();
		model.setFlyBehaviour(new FlyRocketPowered());
		model.performFly();
	}

}
