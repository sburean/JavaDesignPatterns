/*
 * The superclass that contains the template method. (The High-level component)
 * It is defined as final so subclasses can't alter the encapsulated algorithm.
 * Steps of the algorithm that vary are defined as abstract; subclasses will implement specifics.
 * Steps of the algorithm that are common across multiple classes are defined in the superclass. 
 * 
 * IMPORTANT:
 * - each step of the algorithm is represented by a separate method. 
 * - the TemplateMethod has the algorithm and protects it
 * - easier maintenance since algorithm is encapsulated to one location
 * - TemplateMethod provides framework for algorithm that other classes can hook/plug into.
 */

public abstract class CaffeineBeverage {

	/**
	 * The template method for the algorithm (preparing a beverage) defined in individual methods
	 * within this abstract superclass. 
	 */
	final void prepareBeverage(){

		boilWater(); //common
		brew(); //varying
		pourInCup(); //common
		if(customerWantsCondiments()){
			/* This step of the algorithm is controlled by a hook; 
			  	runs by default in all subclasses but may be controlled */
			
			addCondiments(); //varying
		} else {
			System.out.println("Complete.\n");
		}
		
	}
	
	/* Varying methods of the algorithm; define as abstract and let subclasses provide specific implementations */
	public abstract void brew();
	
	public abstract void addCondiments();
	
	/* Common methods of the algorithm; define in this superclass */
	public void boilWater(){
		System.out.println("Boiling Water.");
	}
	
	public void pourInCup(){
		System.out.println("Pouring into cup.");
	}

	public boolean customerWantsCondiments(){
		/* A hook method; (fairly) empty method that may be overriden by subclasses
		 	to run some code. In this case, Subclasses may override this to control how the boolean 
		 	is retrieved and therefore controlling when the "addCondiments()" step of the algorithm runs */
		return true;
	}
	
}
