/*
 * A subclass that uses the TemplateMethod to implement the encapsulated algorithm. 
 * It provides specific implementation to varying steps of the algorithm.
 */

public class Coffee extends CaffeineBeverage {

	//Other methods...
	
	/* Specific implementation for the TemplateMethod algorithm */
	
	@Override
	public void brew() {
		System.out.println("Dripping Coffee through filter.");
	}

	@Override
	public void addCondiments() {
		System.out.println("Adding Sugar and Milk.");
	}
		
}
