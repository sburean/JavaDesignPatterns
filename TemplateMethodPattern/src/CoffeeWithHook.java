import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * A subclass that uses the TemplateMethod to implement the encapsulated algorithm. 
 * It provides specific implementation to varying steps of the algorithm.
 * 
 * This subclass shows the usage of the Hook in the superclass to decide -
 * - under what conditions a step of the algorithm runs. (When to add condiments to the drink)
 */

public class CoffeeWithHook extends CaffeineBeverage {

	//Other methods...
	
	/* Specific implementation for the TemplateMethod algorithm */
	
	@Override
	public void brew() {
		System.out.println("Dripping Coffee through filter.");
	}

	@Override
	public void addCondiments() {
		System.out.println("Adding Sugar and Milk.\n");
	}
	
	@Override
	public boolean customerWantsCondiments(){
		/* Overriding the hook to implement custom logic that determines whether a step of the algorithm runs. */
		
		String answer = getUserInput();
		if(answer.toLowerCase().startsWith("y")){
			return true;
		} else {
			return false;
		}
		
	}
	
	public String getUserInput(){
		/* Method to get user input whether they want condiments or not. User will enter -
		  	- y for yes, and n for no. */
		
		String answer = null;
		
		System.out.println("Would you like milk and sugar with your coffee (y/n) ?");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{
			answer = reader.readLine();
		} catch (IOException ioexcept) {
			System.err.println("IO error trying to read your answer");
		}
		
		if(answer == null){
			answer = "no";
		}
		
		return answer;
	}
		
}
