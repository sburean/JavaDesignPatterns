/*
 * The client for the TemplateMethod. It creates the subclass that will use the encapsulated algorithm. 
 * The TemplateMethod is called on the subclass itself to perform the algorithm. 
 */

public class TestStub {
	
	public static void main(String[] args){
		
		Tea myTea = new Tea(); //tea object
		myTea.prepareBeverage(); //call template method; contains the algorithm and executes it. 
		/* NOTE: Template method uses specific implementations of object that call it to supply abstract method implementations. */
		
		/* template method will follow algorithm and execute it based on the object that calls it */
		System.out.println("---------------");
		
		//Now the subclasses with hooks:
		TeaWithHook teaWHook = new TeaWithHook();
		CoffeeWithHook coffeeWHook = new CoffeeWithHook();
		
		teaWHook.prepareBeverage();
		coffeeWHook.prepareBeverage();
		
	}
	
}
