/*
 * A class that is a part of the large, complex subsystem. 
 */

public class DvdPlayer {

	public void on(){
		System.out.println("DvdPlayer on.");
	}
	
	public void off(){
		System.out.println("DvdPlayer off.");
	}
	
	public void eject(){
		System.out.println("DvdPlayer ejected DVD.");
	}
	
	public void pause(){
		System.out.println("DvdPlayer paused.");
	}
	
	public void play(String movie){
		System.out.println("DvdPlayer playing movie: " + movie);
	}
	
	public void setSurroundAudio(){
		System.out.println("DvdPlayer set to surround sound.");
	}
	
	public void setTwoChannelAudio(){
		System.out.println("DvdPlayer set to two channel audio.");
	}
	
	public void stop(){
		System.out.println("DvdPlayer stopped.");
	}
	
}
