/*
 * A class that is a part of the large, complex subsystem. 
 */

public class CdPlayer {

	public void on(){
		System.out.println("CD Player on.");
	}
	
	public void off(){
		System.out.println("CD Player off.");
	}
	
	public void eject(){
		System.out.println("CD Player ejected CD.");
	}
	
	public void pause(){
		System.out.println("CD Player paused.");
	}
	
	public void play(){
		System.out.println("CD Player playing.");
	}
	
	public void stop(){
		System.out.println("CD Player stopped.");
	}
	
}
