/*
 * A class that is a part of the large, complex subsystem. 
 */

public class Projector {

	public void on(){
		System.out.println("Projector on.");
	}
	
	public void off(){
		System.out.println("Projector off.");
	}
	
	public void tvMode(){
		System.out.println("Projector set to TV-mode");
	}
	
	public void wideScreenMode(){
		System.out.println("Projector set to wide-screen mode.");
	}
	
}
