/*
 * A class that is a part of the large, complex subsystem. 
 */

public class Amplifier {

	CdPlayer cdPlayer;
	DvdPlayer dvdPlayer;
	Tuner tuner;
	
	public void on(){
		System.out.println("Amplifier is on.");
	}
	
	public void off(){
		System.out.println("Amplifier is off.");
	}
	
	public void setCD(CdPlayer cdPlayer){
		this.cdPlayer = cdPlayer;
		System.out.println("Amplifier CD player set successfully.");
	}
	
	public void setDVD(DvdPlayer dvdPlayer){
		this.dvdPlayer = dvdPlayer;
		System.out.println("Amplifier DVD player set successfully.");
	}
	
	public void setTuner(Tuner tuner){
		this.tuner = tuner;
		System.out.println("Amplifier tuner set successfully.");
	}
	
	public void setVolume(int volume){
		System.out.println("Volume set to: " + volume);
	}
	
	public void setStereoSound(){
		System.out.println("Amplifier successfully set stereo sound.");
	}
	
	public void setSurroundSound(){
		System.out.println("Amplifier successfully set surround sound.");
	}
}
