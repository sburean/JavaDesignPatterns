/*
 * The facade class. It is used to simplify the interface of a large ( & complex ) subsystem. 
 *  -> "Hides the complexity of one or more classes." 
 * Obviously associated with the large subsystem. 
 * 
 *  NOTES:
 *	- The facade does NOT encapsulate the underlying subsystem. It leaves it accessible to be use directly as needed.
 *	- Facade is free to add own functionality while making use of the subsystem. 
 *	- Any number of facades may be created for a given subsystem. 
 *	- DECOUPLES client from the subsystem! If subsystem changes, client code is not affected.  
 */

public class HomeTheatreFacade {

	//Associated with all subsystem components:
	
	Amplifier amp;
	Tuner tuner; 
	DvdPlayer dvd; 
	CdPlayer cd; 
	Projector projector; 
	TheaterLights lights; 
	Screen screen; 
	PopcornPopper popper;
	
	public HomeTheatreFacade(Amplifier amp, Tuner tuner, DvdPlayer dvdPlayer, CdPlayer cdPlayer, 
			Projector projector, TheaterLights lights, Screen screen, PopcornPopper popcornPopper) {
		
		//Constructor takes references to required subsystem components
		
		this.amp = amp;
		this.tuner = tuner;
		this.dvd = dvdPlayer;
		this.cd = cdPlayer;
		this.projector = projector;
		this.lights = lights;
		this.screen = screen;
		this.popper = popcornPopper;
		
	}
	
	//Other Facade methods: (The simplified interface that the Facade exposes)
	
	public void watchMovie(String movie) { 
		
		/* To perform a task, each task is delegated to the appropriate subsystem */
		
		System.out.println("Get ready to watch a movie..."); 
		popper.on();
		popper.pop();
		lights.dim(10);
		screen.down();
		projector.on();
		projector.wideScreenMode();
		amp.on();
		amp.setDVD(dvd);
		amp.setSurroundSound();
		amp.setVolume(5);
		dvd.on();
		dvd.play(movie);
		
	}
	
	public void endMovie() {
		
		System.out.println("Shutting movie theater down..."); 
		popper.off();
		lights.on();
		screen.up();
		projector.off();
		amp.off();
		dvd.stop();
		dvd.eject();
		dvd.off();
		
	}
	
	//More methods here..
	
}
