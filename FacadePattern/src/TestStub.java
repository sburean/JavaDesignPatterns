/*
 * The client will now simply call methods on the Facade, NOT the subsystem. 
 * Since the Facade doesn't "encapsulate" the subsystem classes, the advanced functionality of the larger, more complex -
 * - subsystem is available to be accessed directly. 
 * 
 * Important to note that the client is DECOUPLED from the underlying subsystem. Only accesses the facade. 
 * 	-> If the underlying subsystem changes, client code is not affected. 
 */

public class TestStub {

	public static void main(String[] args) {
		
		//This test stub instantiates the subsystem components itself. Normally, it is simply given a facade.
		
		Amplifier amp = new Amplifier();
		CdPlayer cd = new CdPlayer();
		DvdPlayer dvd = new DvdPlayer();
		PopcornPopper popcorn = new PopcornPopper();
		Projector projector = new Projector();
		Screen screen = new Screen();
		TheaterLights lights = new TheaterLights();
		Tuner tuner = new Tuner();
		
		//Create facade:
		HomeTheatreFacade facade = new HomeTheatreFacade(amp, tuner, dvd, cd, projector, lights, screen, popcorn);
		
		//Now simply call methods on the facade:
		facade.watchMovie("The Mummy");
		System.out.println("------------");
		facade.endMovie();
		
	}

}
