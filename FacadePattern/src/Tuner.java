/*
 * A class that is a part of the large, complex subsystem. 
 */

public class Tuner {
	
	public void on(){
		System.out.println("Tuner on.");
	}
	
	public void off(){
		System.out.println("Tuner off.");
	}
	
	public void setAm(int val){
		System.out.println("Tuner set to AM channel: " + val);
	}
	
	public void setFm(Double val){
		System.out.println("Tuner set to FM channel: " + val);
	}
	
	public void setFrequency(Double freq){
		System.out.println("Tuner frequency set to: " + freq);
	}
	
}
