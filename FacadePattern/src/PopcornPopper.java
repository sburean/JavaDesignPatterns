/*
 * A class that is a part of the large, complex subsystem. 
 */

public class PopcornPopper {

	public void on(){
		System.out.println("Popcorn popper on.");
	}
	
	public void off(){
		System.out.println("Popcorn popper off.");
	}
	
	public void pop(){
		System.out.println("Popcorn popper popping!");
	}
}
