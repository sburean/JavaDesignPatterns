/*
 * A class that is a part of the large, complex subsystem. 
 */

public class TheaterLights {
	
	public void on(){
		System.out.println("TheatreLights on.");
	}
	
	public void off(){
		System.out.println("TheatreLights off.");
	}
	
	public void dim(int val){
		System.out.println("Dimming theatre lights to " + val + " percent");
	}
}
