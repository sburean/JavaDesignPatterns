/*
 * A class that is a part of the large, complex subsystem. 
 */

public class Screen {
	
	public void up(){
		System.out.println("Screen up.");
	}
	
	public void down(){
		System.out.println("Screen down.");
	}
}
