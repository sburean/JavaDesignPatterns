import java.util.Observable;
import java.util.Observer;

/*
 *  This class is interested in the state of the WeatherData class to properly display information.
 *  It is an observer, and implements the java.util.Observer interface in addition to the DisplayElement interface.
 *  NOTE: This observer only uses the temperature and humidity of the observable object, using a pull-update style.  
 */

public class CurrentConditionsDisplay implements Observer, DisplayElement {

	Observable observable; //used to register ourselves as an observer, AND to pull state from (pull-style notification)
	private float temperature;
	private float humidity;
	
	public CurrentConditionsDisplay(Observable observable) {
		this.observable = observable;
		this.observable.addObserver(this); //possible because we have reference to our observable, otherwise do in client
	}
	
	@Override
	public void display() {
		System.out.println("Current Conditions: " + temperature + "F degrees and " + humidity + "% humidity");		
	}

	@Override
	public void update(Observable obj, Object arg) {
		
		/*
		 * First parameter = subject that sent the notification
		 * Second parameter = data object that was passed (push-style), null if nothing passed (pull-style)
		 * 
		 * NOTE: With this method, we don't really need a reference to our observable for the java.util.observable pkg. 
		 * 		We can use the obj that is passed into the update, as seen below: 
		 */
		
		//First make sure obj is of type WeatherData as expected:
		if(obj instanceof WeatherData){
			//Then use the subject's getters to pull required state:
			WeatherData weatherData = (WeatherData)obj;
			this.temperature = weatherData.getTemperature();
			this.humidity = weatherData.getHumidity();
			
			//Now update the display once state changed:
			display(); 
			
		} else {
			System.err.println("something messed up..");
		}
		
	}

	
	
}
