/*
 * For the purposes of this example, an interface for all display elements to implement.
 * Method is called whenever a display element needs to be displayed. 
 */

public interface DisplayElement {

	public void display();
	
}
