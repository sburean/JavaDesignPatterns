import java.util.Observable;

/*
 * This is the class that holds state and controls it. (The subject/Observable)
 * It extends the built-in Java Observable superclass and inherits a collection of observers with control methods:
 * -> addObserver()
 * -> deleteObserver()
 * -> notifyObservers()
 * -> setChanged/clearChanged/hasChanged()
 * Important to note that the superclass now handles management of the observers collection, and -
 * - implementations of the control methods described above. 
 * 
 * More info here: https://docs.oracle.com/javase/7/docs/api/java/util/Observable.html
 */

public class WeatherData extends Observable {

	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData(){
		/* Empty Constructor; no longer needs to create a data structure to hold observers. */
	}
	
	public void measurementsChanged(){
		
		/*
		 * When state changes, set the changed flag and notify all observers.
		 * NOTE: Because we're not passing in a parameter to notifyObservers, we're using the Pull-Update style.
		 */
		
		setChanged();
		notifyObservers();
	}
	
	public void setMeasurements(float temp, float humidity, float pressure){
		/* A test method to change the WeatherData state for the purposes of this example */
		this.temperature = temp;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	/* Since we're using the pull-update style, we need getters for each state variable  */
	
	public float getTemperature(){
		return this.temperature;
	}
	
	public float getHumidity(){
		return this.humidity;
	}
	
	public float getPressure(){
		return this.pressure;
	}
	
}
