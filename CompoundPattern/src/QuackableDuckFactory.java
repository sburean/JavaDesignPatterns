/*
 * A concrete factory that is responsible for creating a family of products. 
 * All code that deals with instantiating Quackables is encapsulated here.
 */

public class QuackableDuckFactory implements AbstractDuckFactory {

	@Override
	public Quackable getMallardDuck() {
		return new MallardDuck();
	}

	@Override
	public Quackable getRedheadDuck() {
		return new RedheadDuck();
	}

	@Override
	public Quackable getRubberDuck() {
		return new RubberDuck();
	}

	@Override
	public Quackable getDuckCall() {
		return new DuckCall();
	}

}
