/*
 * A concrete class that implements the Quackable interface.(A leaf)
 */

public class RubberDuck implements Quackable {

	//delegate subject functionality to helper class:
	private Observable observable;
	
	public RubberDuck() {
		this.observable = new Observable(this);
	}
	
	@Override
	public void quack() {
		System.out.println("Squeak");
		notifyObservers(); //When this quackable quacks, it needs to notify its observers.
	}
	
	@Override
	public void add(Observer observer) {
		observable.add(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

}
