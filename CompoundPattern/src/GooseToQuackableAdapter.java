/*
 * An adapter to adapt a Goose-interface to a Quackable-interface.
 * Recall:
 * 	- Implements target interface. (what the client expects)
 *  - Uses adaptee interface.
 */
public class GooseToQuackableAdapter implements Quackable {

	private Goose adaptee;
	private Observable observable;
	
	public GooseToQuackableAdapter(Goose goose) {
		this.adaptee = goose;
		this.observable = new Observable(this);
	}
	
	@Override
	public void quack() {
		//Map the Quackable's quack-method to the Goose's honk-method.
		adaptee.honk();
		notifyObservers();
	}

	@Override
	public void add(Observer observer) {
		observable.add(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

}
