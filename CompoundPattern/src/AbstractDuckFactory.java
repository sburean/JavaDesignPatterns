/*
 * An interface for an AbstractFactory. We are creating a family of products (Quackables)
 * 
 * Unless Quackables are wrapped in their decorator, new functionality won't take effect. 
 * So we need to encapsulate object creation into a factory. 
 */

public interface AbstractDuckFactory {

	/*
	 * If multiple concrete products ALL implement the SAME INTERFACE (belong to the same supertype), they are simply v
	 */
	
	public Quackable getMallardDuck();
	public Quackable getRedheadDuck();
	public Quackable getRubberDuck();
	public Quackable getDuckCall();
	
}
