/*
 * A concrete decorator that adds extra functionality to the object it wraps.
 * -> Count the total number of times quack is called.
 * 
 *  IMPORTANT: 
 *  - Remember that objects have to be wrapped with decorators or they won't get the added behaviour! 
 */

public class CountQuackBehaviour extends QuackableDecorator {

	//Inherit object we're wrapping from superclass..
	static int quackCount;
	
	public CountQuackBehaviour(Quackable objToDecorate) {
		super(objToDecorate);
		quackCount = 0;
	}
	
	@Override
	public void quack() {
		super.quack(); //delegate original behaviour to object we're wrapping
		quackCount++; //increment counter for quacks. 
	}
	
	public static int getQuackCount(){
		//Static method that returns the number of quacks in ALL Quackables
		return quackCount;
	}

	@Override
	public void add(Observer observer) {
		super.add(observer); //delegate original behaviour to object we're wrapping
	}

	@Override
	public void notifyObservers() {
		super.notifyObservers(); //delegate original behaviour to object we're wrapping
	}
}
