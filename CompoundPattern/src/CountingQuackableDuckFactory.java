/*
 * A concrete factory that is responsible for creating a family of products. 
 * All code that deals with instantiating Quackables is encapsulated here.
 * 
 * - This concrete factory returns products that are wrapped in a CountQuackBehaviour decorator. 
 */

public class CountingQuackableDuckFactory implements AbstractDuckFactory {

	@Override
	public Quackable getMallardDuck() {
		return new CountQuackBehaviour(new MallardDuck());
	}

	@Override
	public Quackable getRedheadDuck() {
		return new CountQuackBehaviour(new RedheadDuck());
	}

	@Override
	public Quackable getRubberDuck() {
		return new CountQuackBehaviour(new RubberDuck());
	}

	@Override
	public Quackable getDuckCall() {
		return new CountQuackBehaviour(new DuckCall());
	}

}
