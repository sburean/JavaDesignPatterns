/*
 * An interface that all ducks that quack will implement.
 * NOTE: If we want to observe behaviour of ALL Quackables, simply have the Quackable interface (this)
 * 			extend the QuackObservable interface.
 */

public interface Quackable extends QuackObservable {

	public void quack();
	
}
