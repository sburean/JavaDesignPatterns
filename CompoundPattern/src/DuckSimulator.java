/*
 * The simulator. (TestStub)
 */

public class DuckSimulator {
	
	public static void main(String[] args) {
		DuckSimulator simulator = new DuckSimulator();
		AbstractDuckFactory factory = new CountingQuackableDuckFactory(); //polymorphic factory
		simulator.simulate(factory);
	}
	
	public void simulate(AbstractDuckFactory factory){
		
		/*
		 * Polymorphic factory passed in as argument to the simulate() method.  
		 * Based on the concrete factory we instantiate, 
		 * different kinds of products will be created in the simulate() method.
		 * 
		 * NOTE: Goose is still being created as a concrete Type; use a separate factory. (Its quack isn't counted)
		 */
		
		Quackable redheadDuck = factory.getRedheadDuck();
		Quackable duckCall = factory.getDuckCall();
		Quackable rubberDuck = factory.getRubberDuck();
		Quackable goose = new GooseToQuackableAdapter(new Goose()); //wrap goose in an adapter to look like a quackable. 
		
		System.out.println("Duck Simulator: With Observer\n");
		
		Flock flockOfDucks = new Flock(); //Create a composite. NOTE: Used concrete type to access "add" method. 
											//Can add this to quackable interface and use the super-type.
		
		flockOfDucks.addToFlock(redheadDuck);
		flockOfDucks.addToFlock(duckCall);
		flockOfDucks.addToFlock(rubberDuck);
		flockOfDucks.addToFlock(goose);
		
		//Create a small family of mallards
		Flock flockOfMallards = new Flock();
		Quackable mallardOne = factory.getMallardDuck();
		Quackable mallardTwo = factory.getMallardDuck();
		Quackable mallardThree = factory.getMallardDuck();
		Quackable mallardFour = factory.getMallardDuck();
		flockOfMallards.addToFlock(mallardOne);
		flockOfMallards.addToFlock(mallardTwo);
		flockOfMallards.addToFlock(mallardThree);
		flockOfMallards.addToFlock(mallardFour);
		
		//Now add the mallard family to the flock of ducks:
		flockOfDucks.addToFlock(flockOfMallards);
		
		//Create our observer
		Quackologist quackologist = new Quackologist();
		
		//Register the observer with with our flock of ducks.
		flockOfDucks.add(quackologist);
		
		System.out.println("\nDuck Simulator: Whole Flock Simulation");
		simulate(flockOfDucks);
		
//		System.out.println("\nDuck Simulator: Mallard Flock Simulator");
//		simulate(flockOfMallards);
		
		System.out.println("\nThe ducks quacked " + CountQuackBehaviour.getQuackCount() + " times");
	}
	
	public void simulate(Quackable duck){
		/*
		 * Overloaded simulate method for each duck.
		 * NOTE: Using polymorphism. No matter what kind of quackable is passed in, 
		 * 			each one implements a quack method. 
		 */
		
		duck.quack();
	}
	
}
