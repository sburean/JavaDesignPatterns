/*
 * An observer class. It prints out the quackable that quacked.
 */

public class Quackologist implements Observer {

	@Override
	public void update(Quackable subject) {
		System.out.println("Quackologist: " + subject + " just quacked.");
	}

}
