/*
 * A concrete class. Note that this doesn't implement the quackable interface.
 *  
 * If we want our simulator to use this Goose anywhere it uses a Quackable, 
 * 	we'd need an adaptor to adapt this interface to a quackable interface, as the simulator expects. 
 */

public class Goose {
	public void honk(){
		System.out.println("Honk");
	}
}
