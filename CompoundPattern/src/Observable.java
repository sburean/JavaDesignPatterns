import java.util.ArrayList;

/*
 * A helper-class that encapsulates Observer behaviour: 
 * 	-> adding and notifying all observers to a subject. (no code duplication)
 * Subjects will delegate observer behaviour to this class. 
 * 
 * STRATEGY PATTERN!
 * 
 */
public class Observable implements QuackObservable {

	private ArrayList<Observer> observers;
	private Quackable subject; //keep a reference to subject.
	
	public Observable(Quackable quackable) {
		this.observers = new ArrayList<>();
		this.subject = quackable;
	}
	
	@Override
	public void add(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update(subject); //notify objects with the subject itself.
		}
	}

}
