/*
 * A concrete class that implements the Quackable interface. (A leaf)
 */

public class RedheadDuck implements Quackable {

	//delegate subject functionality to helper class:
	private Observable observable;
	
	public RedheadDuck() {
		this.observable = new Observable(this);
	}
	
	@Override
	public void quack() {
		System.out.println("Quack");
		notifyObservers(); //When this quackable quacks, it needs to notify its observers.
	}

	@Override
	public void add(Observer observer) {
		observable.add(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

}
