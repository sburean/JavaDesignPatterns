/*
 * An interface that all observers will implement. 
 * Contains one method that will be called by the subject when its state changes. 
 * Observers will then take appropriate action. 
 */
public interface Observer {

	public void update(Quackable subject); //parameter is the quackable that changes it's state.
	
}
