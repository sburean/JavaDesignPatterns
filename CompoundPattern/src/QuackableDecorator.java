/*
 * AN abstract decorator to our Quackables. 
 * Concrete decorators will add extra behaviour to Quackables.  
 */

public abstract class QuackableDecorator implements Quackable {

	protected Quackable objToDecorate;
	
	public QuackableDecorator(Quackable objToDecorate) {
		this.objToDecorate = objToDecorate;
	}
	
	//delegate original behaviour to object we're decorating.
	@Override
	public void quack(){
		objToDecorate.quack();
	}
	
	@Override
	public void add(Observer observer) {
		objToDecorate.add(observer);
	}

	@Override
	public void notifyObservers() {
		objToDecorate.notifyObservers();
	}

}
