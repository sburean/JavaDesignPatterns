/*
 * An interface that all subjects (Quackables) will implement. 
 * (Used to observe behaviour of certain objects: subjects)
 * 
 * Contains methods to add and notify observers.
 */

public interface QuackObservable {
	
	public void add(Observer observer);
	public void notifyObservers();

}
