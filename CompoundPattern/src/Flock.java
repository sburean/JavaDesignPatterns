import java.util.ArrayList;

/*
 * A composite. It contains a collection of components (Quackables)
 * Using the composite pattern, a composite may be treated as an individual component. (A Quackable)
 * NOTE:
 * 	It implements the same interface as the components and contains a collections of components.  
 */

public class Flock implements Quackable {
	
	//Composite contains collections of components, which may be other composites themselves. 
	private ArrayList<Quackable> quackers;
	
	public Flock() {
		quackers = new ArrayList<>();
	}

	public void addToFlock(Quackable quacker){
		//Adds a component to this composite's component collection.
		quackers.add(quacker);
	}
	
	@Override
	public void quack() {
		// Each Quackable in this flock quacks 
		for (Quackable quackable : quackers) {
			/*
			 * NOTE: Recursion here, if a component is a composite itself, 
			 * 			it will call of it's component's quack() method, and so on...
			 */
			quackable.quack();
		}

	}

	@Override
	public void add(Observer observer) {
		//When an observer wants to observe a composite, it registers as an observer with ALL of its children:
		for (Quackable quackable : quackers) {
			quackable.add(observer);			
		}
	}

	@Override
	public void notifyObservers() {

		/*
		 * IMPORTANT: This code is empty, because each Quackable does its own notification when Quack() is called.
		 * 				(This happens when flock delegates quack() to each Quackable in the flock)
		 * 				Therefore, the flock doesn't actually need to implement this. 
		 */
		
//		for (Quackable quackable : quackers) {
//			quackable.notifyObservers();			
//		}
		
	}

	
}