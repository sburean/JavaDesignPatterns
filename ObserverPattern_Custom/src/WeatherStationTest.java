/*
 * This is a test stub for the Observer Pattern. 
 * We create a subject and observer, register the observer with the subject -
 * - and then update the state of the subject. The observer will update -
 * - its display accordingly.  
 */

public class WeatherStationTest {

	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		
		/* NOTE: Automatically registers with subject inside constructor; will display state of change automatically */
		CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);  
		
		weatherData.setMeasurements(80, 65, 30.4f);
	}

}
