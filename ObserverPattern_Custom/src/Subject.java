/*
 * THis is the subject interface. It contains three methods to:
 * 1: Allow observers to register themselves with the subject.
 * 2: Allow observers to remove themselves from the subject.
 * 3: For the subject to notify its observers that a change of state occurred. 
 */

public interface Subject {

	public void registerObserver(Observer o);
	
	public void removeObserver(Observer o);
	
	public void notifyObservers();
	
}
