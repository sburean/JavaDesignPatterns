/*
 *  This class is interested in the state of the WeatherData class to properly display information.
 *  It is an observer, and implements the observer interface. 
 *  NOTE: Since the update method has parameters, this observer-pattern uses the push-update style. 
 */

public class CurrentConditionsDisplay implements Observer, DisplayElement {

	private float temperature;
	private float humidity;
	private Subject weatherData; //A reference to the subject. (association relationship) Handy when we would want to unregister this object as an observer, otherwise don't need. 
	
	public CurrentConditionsDisplay(Subject weatherData){
		
		/* 
		 * NOTE: Constructor takes a subject as an argument. 
		 * This can be modified by adding setters to allow adding a subject at a later time. 
		 */
		
		this.weatherData = weatherData;
		weatherData.registerObserver(this); //register this as an observer as soon as it's created
	}
	
	@Override
	public void display() {
		System.out.println("Current Conditions: " + temperature + "F degrees and " + humidity + "% humidity");
	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		/* 
		 * Called by the subject to indicate a change of state. This observer then updates it's own state accordingly.
		 * NOTE: Not all parameters are used by this observer, might be more useful for it to pull the state it needs.
		 * (For pulling-updates, need to write getters in the subject) 
		 */
		this.temperature = temp;
		this.humidity = humidity;
		display(); //call the display method to display new state to user(s)
	}

}
