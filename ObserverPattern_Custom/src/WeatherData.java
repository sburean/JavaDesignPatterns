import java.util.ArrayList;

/*
 * This is the class that holds state and controls it. (The subject)
 * It contains a collection of observers that are interested in, and use the state of this class.
 */

public class WeatherData implements Subject {

	private ArrayList<Observer> observers;
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData(){
		observers = new ArrayList<>();
	}
	
	public void measurementsChanged(){
		
		/* This is called when the state of the WeatherData (subject) is updated: notify observers */
		notifyObservers();
	}
	
	public void setMeasurements(float temp, float humidity, float pressure){
		/* A test method to change the WeatherData state for the purposes of this example */
		
		this.temperature = temp;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void notifyObservers() {
		for (Observer observer : observers) {
			observer.update(temperature, humidity, pressure);
		}
	}

}
