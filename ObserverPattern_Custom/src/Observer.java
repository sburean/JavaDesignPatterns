/*
 * This is the observer interface. 
 * It contains one method that the subject calls on a state change to update observers with new values.
 * 
 * NOTE: Parameters to this method reflect what value(s) are passed to observers on a state change, however these may be -
 * - subject to change -> Need to encapsulate them!
 */

public interface Observer {

	/* Parameters are the state values the observers get from the subject. See NOTE above */
	
	//TODO: state values being passed to observers is subject to change -> Need to encapsulate.
	public void update(float temp, float humidity, float pressure); 
	
}
